#!/usr/bin/env tdaq_python
# created by Arthur.Bolz@cern.ch
# 18 Nov 2015
# creates file containing eformat::FullEventFragments with FTK events.
# based on makeFullEventFile.py by Andre Anjos <andre.dos.anjos@cern.ch>  

"""Implements constructions to create dummy events and data
"""

import random
import eformat
import sys, getopt

def make_rob(source_id, payload=-1,
             lvl1_id=0, bc_id=0, status=[0x0]):
  """Generates a ROB fragment with random content.

  This method will generate a eformat.write.ROBFragment with random content.

  Parameters:

  source_id -- This is the full source identifier to be used for the created
  ROBFragment.

  payload -- This can be either an integer number, indicating the number of
  random words to generate as the payload of the (ROD) fragment or a list,
  indicating the precise contents of the fragment. If this number is smaller
  than zero, I'll will decide how much data to put in the ROB (between 1 and
  500 words).

  lvl1_id -- This is to set the Level-1 identifier of the ROB.
  
  bc_id -- This is good to set the Bunch Crossing identifier of this ROB.

  status -- Set here a status list, if you want to have anything different than
  [0x0] (the default).

  Returns a eformat.write.ROBFragment
  """
  retval = eformat.write.ROBFragment()
  if type(payload) not in [list, eformat.u32list, eformat.u32slice]: 
    if payload < 0: payload = random.randint(1, 500)
    payload = [random.randint(0, 0xffffffff) for k in range(payload)]
  retval.rod_data(payload)
  retval.rod_lvl1_id(lvl1_id)
  retval.rod_bc_id(bc_id)
  retval.status(status)
  retval.source_id(source_id)
  return retval

def make_ftk_rob(source_id, nTrks=-1, lvl1_id=0, bc_id=0, status=[0x0]):
  """Generates a ROB fragment in FTK format with random content.

  This method will generate a eformat.write.ROBFragment with random content.

  Parameters:
  
  source_id -- This is the full source identifier to be used for the created
  ROBFragment.

  nTrks -- Integer number, indicating the number of FTK tracks to generate as 
  the payload of the (ROD) fragment. If this number is smaller than zero, I'll 
  will decide how many tracks to put in the ROB (between 1 and 50 tracks).

  lvl1_id -- This is to set the Level-1 identifier of the ROB.
  
  bc_id -- This is good to set the Bunch Crossing identifier of this ROB.

  status -- Set here a status list, if you want to have anything different than
  [0x0] (the default).

  Returns a eformat.write.ROBFragment
  """
  formatVersion         = 2.2
  trkSize               = 21  # 22
  trackBlockOffsetStart = 0   # 0
  trackBlockOffsetEnd   = 0   # 6
  if formatVersion == 1:
    trkSize               = 21
    trackBlockOffsetStart = 0  
    trackBlockOffsetEnd   = 0
  if formatVersion == 2.2:
    trkSize               = 22
    trackBlockOffsetStart = 0 
    trackBlockOffsetEnd   = 6

  retval = eformat.write.ROBFragment()
  if nTrks < 0: nTrks = random.randint(1, 50)
  data    = [random.randint(0, 0xffffffff) for k in range(nTrks * trkSize)]
  payload = [0 for i in range(trackBlockOffsetStart)] + data + [0 for i in range(trackBlockOffsetEnd)] 
  if formatVersion == 1:
    payload = [nTrks] + payload
  retval.rod_data(payload)
  retval.rod_lvl1_id(lvl1_id)
  retval.rod_bc_id(bc_id)
  retval.status(status)
  retval.source_id(source_id)
  return retval
 
def make_fe(payload=-1, lvl1_id=0, bc_id=0, status=[0x0], onlyFTK=False):
  """Generates a FullEvent fragment with random content.

  This method will generate a eformat.write.FullEventFragment with random
  content.

  Parameters:

  payload -- This can be either an integer number, indicating the number of
  random ROBs to generate or a list, indicating the precise contents of
  the fragment in terms of eformat.write.ROBFragment's. If the value is
  set to a value smaller than zero, I will choose how many subfragments this
  event will have.

  lvl1_id -- This is to set the Level-1 identifier of itself and the underlying
  fragments.
  
  bc_id -- This is good to set the Bunch Crossing identifier for the underlying
  fragments.

  status -- Set here a status list, if you want to have anything different than
  [0x0] (the default).

  onlyFTK -- If this flag is set to True, will only generate FTK ROB fragments.

  Returns a eformat.write.FullEventFragment
  """
  retval = eformat.write.FullEventFragment()
  if type(payload) not in [list]:
    if payload < 0: payload = random.randint(0, 1600)
    payload_ftk = random.randint(1, min(payload, 16)) # max 16 ftk module_ids
    if onlyFTK: payload_ftk = min(payload, 16)
    sid_ftk = [eformat.helper.SourceIdentifier(eformat.helper.SubDetector.TDAQ_FTK, k) for k in range(payload_ftk)]
    detValues = eformat.helper.SubDetector.values.values()
    detValues.remove(eformat.helper.SubDetector.TDAQ_FTK)
    sid_rnd = [eformat.helper.SourceIdentifier(random.choice(detValues), random.choice(xrange(0xffff))) for k in range(payload-payload_ftk)]
    payload = [make_rob(sid_rnd[k],-1,lvl1_id, bc_id) for k in range(payload-payload_ftk)] + [make_ftk_rob(sid_ftk[k], -1,lvl1_id,bc_id) for k in range(payload_ftk)]
  for k in payload: retval.append(k)
  retval.status(status)
  retval.source_id(eformat.helper.SourceIdentifier(eformat.helper.SubDetector.FULL_SD_EVENT, 0x0))
  retval.lvl1_id(lvl1_id)
  return retval

def make_file(filename, events, nROBs, verbose=False, onlyFTK=False):
  """Generates an EventStorage file with as many (dummy) events as you want.

  This method will generate a number of dummy events and store them in the
  file you designate. If specified, events will only contain FTK ROB fragments.

  Parameters:

  filename -- This is the name of the output file you want to have
 
  events -- This is the number of random events to be generated. Level-1
  identifiers will be sequential starting from zero. Bunch Crossing identifier
  will be set to zero in all events. The same is valid for the RunNumber.

  verbose -- If this flag is set to True, this function will print a '.' (dot)
  for every generated fragment.

  onlyFTK -- If this flag is set to True, will only generate FTK ROB fragments.
  """
  import os, sys
  f = eformat.ostream()
  if verbose: 
    sys.stdout.write('Generating %d events => \'%s\'\n' % (events, filename))
    sys.stdout.flush()
  for k in range(events): 
    f.write(make_fe(lvl1_id=k, onlyFTK=onlyFTK, payload=nROBs))
    if verbose: 
      sys.stdout.write('.')
      sys.stdout.flush()
  if verbose: 
    sys.stdout.write('\n')
    sys.stdout.flush()
  tmp = f.last_filename()
  del f
  os.rename(tmp, filename)


#Main
def description():
  print 'usage: makeFTKEventFile.py [-o <outputfile>] [-n <nEvt>] [-f]  [-v]'
  print 'options:'
  print '-o outputfile  Output file (=FTKEventFile.dat)'
  print '-n nEvt        Number of events (=1)'
  print '-m nROBs       Number of ROBs / event, if -1, random number of ROBs generated (=-1)'
  print '-f             If set, only FTK ROB fragments are produced.'
  print '               Per default they are mixed in with other detectors.'
  print '-v             Verbose output'

def main(argv):
  outputfile = 'FTKEventFile.dat'
  nEvt = 1
  nROBs = -1
  onlyFTK = False
  verbose = False
   
  try:
    opts, args = getopt.getopt(argv,"ho:n:m:fv")
  except getopt.GetoptError:
    sys.exit(2)
  for opt, arg in opts:
     if opt == '-h':
       description()
       sys.exit()
     elif opt in ("-o"):
       outputfile = arg
     elif opt in ("-n"):
       nEvt = int(arg)
     elif opt in ("-m"):
       nROBs = int(arg)
     elif opt in ("-f"):
       onlyFTK = True
     elif opt in ("-v"):
       verbose = True
  make_file(outputfile, nEvt, nROBs, verbose, onlyFTK)

if __name__ == "__main__":
  main(sys.argv[1:])
