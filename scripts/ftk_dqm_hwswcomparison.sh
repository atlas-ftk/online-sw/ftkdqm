#!/bin/bash

#########################################
# --- check input arguments
#########################################
if [[ -z $9 ]]; then
    echo "Error. 9 arguments have to be passed to run the script $0."
    exit 4
fi

#########################################
# --- parse input arguments
#########################################
# testarea=$1
# filearea=$2
# interval=$3
# hosts=$4
# apps=$5
# towers=$6
# FTKSetupTag=$7
# ConstantsVersion=$8
# maxEvents=$9



while getopts ":ha:F:T:i:H:t:c:m:s:" opt; do                                                                                                                                                                                                                                                                                                                                                
        case ${opt} in                                                                                                                                                                                                                                                                                                                                                                      
                a ) apps=$OPTARG
                        ;;
                F ) filearea=$OPTARG
                        ;;
                T ) testarea=$OPTARG
                        ;;
                i ) interval=$OPTARG
                        ;;
                H ) hosts=$OPTARG
                        ;;
                t ) towers=$OPTARG
                        ;;
                c ) ConstantsVersion=$OPTARG
                        ;;
                m ) maxEvents=$OPTARG
                        ;;
                s ) FTKSetupTag=$OPTARG
                        ;;
                : ) echo "Invalid option: $OPTARG requires an argument" 1>&2
                        exit 1
                        ;;
                h ) echo -e "Usage:\n   -a : number of apps\n   -F : name of filearea\n         -T : name of testarea\n         -t : number of towers\n         -i : interval between apps (sec)\n      -H : number of hosts\n  -c : ConstantsVersion name\n    -m : max events to proceed\n    -s : FTKSetupTag name\n"
                        exit 1
                        ;;
                \? ) echo "Usage: cmd [-t] [-a] [-F] [-T] [-t] [-i] [-H] [-c] [-m] [-s] or [-h] for help"
                        exit 1
                        ;;
        esac
done
shift $((OPTIND -1))




echo ""
echo "-------------------------------------------"
echo "--- running: ftk_dqm_hwswcomparison.sh"
echo "[$0] Input arguments are"
echo "PWD:               $PWD"
echo "Shell command      $0"
echo "testarea           $testarea"
echo "filearea           $filearea"
echo "interval           $interval"
echo "hosts              $hosts"
echo "apps               $apps"
echo "towers             $towers"
echo "FTKSetupTag        $FTKSetupTag"
echo "ConstantsVersion   $ConstantsVersion"
echo "maxEvents          $maxEvents"
echo "-------------------------------------------"

######################################
##### SETUP trapping and cleanup #####
######################################

killtree() {
    local _pid=$1
    local _sig=${2:--TERM}
    kill -stop ${_pid} # needed to stop quickly forking parent from producing children between child killing and parent killing
    for _child in $(ps -o pid --no-headers --ppid ${_pid}); do
        killtree ${_child} ${_sig}
    done
    kill -${_sig} ${_pid}
}



do_cleanup()
{
  echo -n "Start cleanup on TERM signal on "
  date --rfc-3339=ns

  if [ "$1" ]; then
    echo "children: $1"
    echo "parent: $$"

    #send a TERM signal to the whole children tree
    killtree $1 SIGTERM
    #sleep a short period of time to let children finish
    sleep 1
    #kill brutally, whomever didn't wrap up yet
    killtree $1 SIGKILL
  fi
  ls
  pwd

  rm -rf $testarea/run_$tstamp
  ls
}
#implement cleanup here, for example: killing children, remove temporary files, etc.




#Traps `kill -15` (send by RunControl) and `kill -2` (=Crtl+C)
#It gets pid of the child (or children?) to be killed. Exits on trapping!
#See http://cern.ch/go/sk86 for a nice intro example go google it
ipid=''
trap 'do_cleanup $pid; exit' SIGTERM SIGINT

# trap do_cleanup SIGTERM SIGINT INT TERM EXIT
#######################
##### SETUP delay #####
#######################

if test -z "${TDAQ_APPLICATION_NAME}" ; then echo "TDAQ_APPLICATION_NAME is undefined. Not sleeping." ; exit 0 ; fi

delay=20   # Default

for (( h=0; h<$hosts; h++ ))
do
for (( a=0; a<$apps; a++ ))
do
case "${TDAQ_APPLICATION_NAME}" in

    FTKHLMonitoring:*:pc-tdq-mon-$(($h + 26)):$(($a + 1)) )        delay=$((($interval * $a) + ($interval * $h * $apps) + 20)) ;;

esac
done
done

echo -n "Sleeping for ${delay} seconds ..."
sleep "${delay}" &
wait
echo " Done."

#########################################
##### SETUP ENVIRONMENT & EXECUTION #####
#########################################

echo -n "Checking working partitions:"
ipc_ls -P -l
echo "Moving to ${testarea} ..."
cd $testarea


ftk_jo_path_patch=`echo $PATH | awk -F : '{print $1}' | sed -e "s|bin|data/ftkdqm/jobOptions|g"`
ftk_jo_path=`echo $PATH | awk -F : '{print $3}' | sed -e "s|bin|data/ftkdqm/jobOptions|g"`

########################
##### Athena setup #####
########################
cat <<EOF > myAsetup
[defaults]
tags                    = AthenaP1
release                 = 21.1.7
runtime                 = True
32bit                   = False
os                      = slc6
compiler                = gcc62
optimised               = True
testarea                = $PWD

[environment]
CORAL_DBLOOKUP_PATH=/sw/DbSuppForMx/lookup4oracle
CORAL_AUTH_PATH=/sw/DbSuppForMx/lookup4oracle

[epilog.sh]
export CORAL_DBLOOKUP_PATH=/sw/DbSuppForMx/lookup4oracle
export CORAL_AUTH_PATH=/sw/DbSuppForMx/lookup4oracle
EOF

. /sw/atlas/AtlasSetup/scripts/asetup.sh --input=myAsetup
export JOBOPTSEARCHPATH=$JOBOPTSEARCHPATH:$ftk_jo_path_patch:$ftk_jo_path



#####################################
while :
do
    tstamp=`date +%d%m%Y_%H%M%S`
    mkdir run_$tstamp
    cd run_$tstamp


#####################################
############# TrigFTK ###############
#####################################
########## preinclude file ##########

cat <<EOF > TrigFTK_preinclude.py
rec.AutoConfiguration = []
rec.doCaloRinger=False
rec.doCalo=False
rec.doInDet=True
rec.doMuon=False
rec.doJetMissingETTag=False
rec.doEgamma=False
rec.doMuonCombined=False
rec.doTau=False
rec.doTrigger=False
rec.UserAlgs=["FastTrackSimWrap/FastTrackSimRegionalWrap_${towers}TowersIBL3DTest_jobOptions.py"]

athenaCommonFlags.isOnline.set_Value_and_Lock(True)
athenaCommonFlags.isOnlineStateless.set_Value_and_Lock(True)

from AthenaCommon.GlobalFlags import globalflags
globalflags.ConditionsTag.set_Value_and_Lock("CONDBR2-HLTP-2016-01")
globalflags.DataSource = "data"

from AthenaCommon.DetFlags import DetFlags
DetFlags.all_setOn()
DetFlags.makeRIO.TRT_setOff()
DetFlags.TRT_setOff()
DetFlags.detdescr.TRT_setOn()
DetFlags.dcs.TRT_setOff()
DetFlags.makeRIO.Tile_setOff()
DetFlags.Tile_setOff()
DetFlags.detdescr.Tile_setOff()
DetFlags.dcs.Tile_setOff()

from InDetRecExample.InDetJobProperties import InDetFlags
InDetFlags.pixelClusterSplittingType = "AnalogClus"
InDetFlags.doPixelClusterSplitting=False
InDetFlags.useDCS=False
InDetFlags.doTIDE_Ambi=False
InDetFlags.doParticleCreation=True
InDetFlags.doVertexFinding=False

from xAODTrackingCnv.xAODTrackingCnvConf import xAODMaker__TrackParticleCnvAlg
EOF



#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#:::::::::::::::::::::::::: START of joboptions creation ::::::::::::::::::::::::::::::::::::::::::::::::::::::::

write_runargsRAWtoESD(){
cat <<EOF > runargs.RAWtoESD.py
# JobTransform: RAWtoESD
# Version: $Id$
# Import runArgs class
from PyJobTransforms.trfJobOptions import RunArguments
runArgs = RunArguments()
runArgs.trfSubstepName = 'RAWtoESD' 

runArgs.ConstantsDir = '${filearea}'
runArgs.FTKSetupTag = '${FTKSetupTag}'
runArgs.NSubRegions = 1
runArgs.PixelClusteringMode = 101
runArgs.CachedBank = True
runArgs.FixEndCapL0 = False
runArgs.maxEvents = ${maxEvents}
runArgs.PatternsVersion = '${ConstantsVersion}'
runArgs.DuplicateGanged = 1
runArgs.preInclude = ['./TrigFTK_preinclude.py']
runArgs.GangedPatternReco = 0
runArgs.FitConstantsVersion = '${ConstantsVersion}'

# Input data
runArgs.inputBSFile = ['./out._0001.data']
runArgs.inputBSFileType = 'BS'
runArgs.inputBSFileNentries = 10
runArgs.BSFileIO = 'input'

# Output data
runArgs.outputNTUP_FTKIPFile = 'OUT.NTUP_FTKIP.root'
runArgs.outputNTUP_FTKIPFileType = 'ntup_ftkip'

# Extra runargs

# Extra runtime runargs

# Literal runargs snippets
EOF

}



write_runwrapperRAWtoESD(){
cat <<EOF > runwrapper.RAWtoESD.sh
#! /bin/sh                                                                                                                                                                                                         
# Customised environment                                                                                                                                                                                           
athena.py --preloadlib=/sw/atlas/AthenaExternals/21.1.7/InstallArea/x86_64-slc6-gcc62-opt/lib/libintlc.so.5:/sw/atlas/AthenaExternals/21.1.7/InstallArea/x86_64-slc6-gcc62-opt/lib/libimf.so --drop-and-reload runargs.RAWtoESD.py RecJobTransforms/skeleton.RAWtoESD_tf.py
EOF
}



write_FTKFullSimulationBank(){


#####################################
############# TrigFTK ###############
#####################################
## FTKFullSimulationBank joboptions##

for (( i = 0; i < ${towers}; i++ )); do

cat <<EOF > runargs.FTKFullSimulationBank0$i.py
# JobTransform: FTKFullSimulationBank0${i}
# Version: $Id$
# Import runArgs class
from PyJobTransforms.trfJobOptions import RunArguments
runArgs = RunArguments()
runArgs.trfSubstepName = 'FTKFullSimulationBank0${i}'

runArgs.ConstantsDir = '${filearea}'
runArgs.FTKSetupTag = '${FTKSetupTag}'
runArgs.NSubRegions = 1
runArgs.PixelClusteringMode = 101
runArgs.CachedBank = True
runArgs.FixEndCapL0 = False
runArgs.maxEvents = ${maxEvents}
runArgs.PatternsVersion = '${ConstantsVersion}'
runArgs.DuplicateGanged = 1
runArgs.athenaopts = ['--preloadlib=/sw/atlas/AthenaExternals/21.1.7/InstallArea/x86_64-slc6-gcc62-opt/lib/libimf.so:/sw/atlas/AthenaExternals/21.1.7/InstallArea/x86_64-slc6-gcc62-opt/lib/libintlc.so.5']
runArgs.GangedPatternReco = 0
runArgs.FitConstantsVersion = '${ConstantsVersion}'

# Input data
runArgs.inputNTUP_FTKIPFile = ['OUT.NTUP_FTKIP.root']
runArgs.inputNTUP_FTKIPFileType = 'ntup_ftkip'
runArgs.inputNTUP_FTKIPFileNentries = 10L
runArgs.NTUP_FTKIPFileIO = 'output'

# Output data
runArgs.outputNTUP_FTKTMP_0${i}File = 'tmp.NTUP_FTKTMP_0${i}'
runArgs.outputNTUP_FTKTMP_0${i}FileType = 'ntup_ftk'

# Extra runargs
runArgs.bankregion = [$i]
runArgs.banksubregion = [0]

# Extra runtime runargs
try:
    runArgs.sectorpath = [runArgs.sectorspath[$i]]
except AttributeError:
    print "WARNING - AttributeError for sectorpath"
try:
    runArgs.patternbankpath = [runArgs.patternbankspath[$i]]
except AttributeError:
    print "WARNING - AttributeError for patternbankpath"
try:
    runArgs.fit711constantspath = [runArgs.fit711constantspath[$i]]
except AttributeError:
    print "WARNING - AttributeError for fit711constantspath"
try:
    runArgs.outputNTUP_FTKTMPFile = runArgs.outputNTUP_FTKTMP_0${i}File
except AttributeError:
    print "WARNING - AttributeError for outputNTUP_FTKTMPFile"
try:
    runArgs.bankregion = [$i]
except AttributeError:
    print "WARNING - AttributeError for bankregion"
try:
    runArgs.fitconstantspath = [runArgs.fitconstantspath[$i]]
except AttributeError:
    print "WARNING - AttributeError for fitconstantspath"
try:
    runArgs.subregions = [$i]
except AttributeError:
    print "WARNING - AttributeError for subregions"

# Literal runargs snippets"

EOF

done
}


write_runwrapper_FTKFullSimulationBank(){

#####################################
############# TrigFTK ###############
#####################################
##### FTKFullSimulation wrapper #####


for (( i = 0; i < ${towers}; i++ )); do

cat <<EOF > runwrapper.FTKFullSimulationBank0${i}.sh
#! /bin/sh
# AthenaMP explicitly disabled for this executor
export ATHENA_PROC_NUMBER=0
# Customised environment
athena.py --preloadlib=/sw/atlas/AthenaExternals/21.1.7/InstallArea/x86_64-slc6-gcc62-opt/lib/libimf.so:/sw/atlas/AthenaExternals/21.1.7/InstallArea/x86_64-slc6-gcc62-opt/lib/libintlc.so.5 --drop-and-reload runargs.FTKFullSimulationBank0${i}.py TrigFTKSim/skeleton.FTKStandaloneSim.py
EOF
done
}



write_runargsFTKSimulationMerge(){

cat <<EOF > runargs.FTKSimulationMergeFinal.py
# JobTransform: FTKSimulationMergeFinal
# Version: $Id$
# Import runArgs class
from PyJobTransforms.trfJobOptions import RunArguments
runArgs = RunArguments()
runArgs.trfSubstepName = 'FTKSimulationMergeFinal' 

runArgs.ConstantsDir = '${filearea}'
runArgs.FTKSetupTag = '${FTKSetupTag}'
runArgs.NSubRegions = 1
runArgs.PixelClusteringMode = 101
runArgs.CachedBank = True
runArgs.FixEndCapL0 = False
runArgs.maxEvents = ${maxEvents}
runArgs.PatternsVersion = '${ConstantsVersion}'
runArgs.DuplicateGanged = 1
runArgs.athenaopts = ['--preloadlib=/sw/atlas/AthenaExternals/21.1.7/InstallArea/x86_64-slc6-gcc62-opt/lib/libimf.so:/sw/atlas/AthenaExternals/21.1.7/InstallArea/x86_64-slc6-gcc62-opt/lib/libintlc.so.5']
runArgs.GangedPatternReco = 0
runArgs.FitConstantsVersion = '${ConstantsVersion}'

# Input data
runArgs.inputNTUP_FTKTMP_00File = ['tmp.NTUP_FTKTMP_00']
runArgs.inputNTUP_FTKTMP_00FileType = 'ntup_ftk'
runArgs.inputNTUP_FTKTMP_00FileNentries = 10L
runArgs.NTUP_FTKTMP_00FileIO = 'temporary'
runArgs.inputNTUP_FTKTMP_01File = ['tmp.NTUP_FTKTMP_01']
runArgs.inputNTUP_FTKTMP_01FileType = 'ntup_ftk'
runArgs.inputNTUP_FTKTMP_01FileNentries = 10L
runArgs.NTUP_FTKTMP_01FileIO = 'temporary'
runArgs.inputNTUP_FTKTMP_02File = ['tmp.NTUP_FTKTMP_02']
runArgs.inputNTUP_FTKTMP_02FileType = 'ntup_ftk'
runArgs.inputNTUP_FTKTMP_02FileNentries = 10L
runArgs.NTUP_FTKTMP_02FileIO = 'temporary'
runArgs.inputNTUP_FTKIPFile = ['OUT.NTUP_FTKIP.root']
runArgs.inputNTUP_FTKIPFileType = 'ntup_ftkip'
runArgs.inputNTUP_FTKIPFileNentries = 10L
runArgs.NTUP_FTKIPFileIO = 'output'

# Output data
runArgs.outputNTUP_FTKFile = 'OUT.NTUP_FTK.root'
runArgs.outputNTUP_FTKFileType = 'ntup_ftk'

# Extra runargs

# Extra runtime runargs
try:
    runArgs.SaveTruthTree = 1
except AttributeError:
    print "WARNING - AttributeError for SaveTruthTree"
try:
    runArgs.EvtInfoTreeName = 'evtinfo'
except AttributeError:
    print "WARNING - AttributeError for EvtInfoTreeName"
try:
    runArgs.UnmergedFormatName = 'FTKTracksStream%u.'
except AttributeError:
    print "WARNING - AttributeError for UnmergedFormatName"
try:
    runArgs.FirstRegion = 0
except AttributeError:
    print "WARNING - AttributeError for FirstRegion"
try:
    runArgs.TruthTrackTreeName = 'truthtracks'
except AttributeError:
    print "WARNING - AttributeError for TruthTrackTreeName"
try:
    runArgs.UnmergedRoadFormatName = 'FTKRoadsStream%u.'
except AttributeError:
    print "WARNING - AttributeError for UnmergedRoadFormatName"
try:
    runArgs.MergeFromTowers = True
except AttributeError:
    print "WARNING - AttributeError for MergeFromTowers"

# Literal runargs snippets

EOF
}


write_runwrapper_FTKSimulationMerge(){

cat <<EOF > runwrapper.FTKSimulationMergeFinal.sh
#! /bin/sh                                                                                                                                                                                                         
# AthenaMP explicitly disabled for this executor                                                                                                                                                                   
export ATHENA_PROC_NUMBER=0                                                                                                                                                                                        
# Customised environment                                                                                                                                                                                                                     
athena.py --preloadlib=/sw/atlas/AthenaExternals/21.1.7/InstallArea/x86_64-slc6-gcc62-opt/lib/libimf.so:/sw/atlas/AthenaExternals/21.1.7/InstallArea/x86_64-slc6-gcc62-opt/lib/libintlc.so.5 --drop-and-reload runargs.FTKSimulationMergeFinal.py /atlas-home/1/amaranti/updated_partition/skeleton.FTKStandaloneMerge.py
EOF
}


#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#:::::::::::::::::::::::::: END of joboptions creation ::::::::::::::::::::::::::::::::::::::::::::::::::::::::


## --- sample events from ATLAS
ftk_emon_task.py -p $TDAQ_PARTITION -t dcm -f out -e 10 -D 10000 2>&1 | tee emon.log
# ftk_emon_task.py -p ATLAS -t dcm -f out -e $maxEvents -D 10000 -m 4 -w 100 -N express 2>&1 | tee emon.log


#:::::::::::::::::::::::::::::: run FTKSim - joboptions ::::::::::::::::::::::::::::::#
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::#

echo ""
echo "--- [ftk_dqm_hwswcomparison.sh] Running FTKSim"
touch empty.bmap
    if [[ $towers == 32 ]]; then
        TrigFTKTM32SM1Un_tf.py --maxEvents $maxEvents --inputBSFile ./out._0001.data \
        --ConstantsDir /data/FTK/FTKSimInputs --CachedBank True --FitConstantsVersion $ConstantsVersion --PatternsVersion $ConstantsVersion \
        --outputNTUP_FTKIPFile OUT.NTUP_FTKIP.root --outputNTUP_FTKFile OUT.NTUP_FTK.root --outputBS_FTKFile OUT.BS_FTK.root \
        --FTKSetupTag $FTKSetupTag --PixelClusteringMode 101 --FixEndCapL0 False \
        --preInclude r2e:./TrigFTK_preinclude.py &
       pid="$!" 
       wait
    elif [[ $towers == 64 ]]; then
        TrigFTKTM64SM1Un_tf.py --maxEvents $maxEvents --inputBSFile ./out._0001.data \
        --ConstantsDir /data/FTK/FTKSimInputs --CachedBank True --FitConstantsVersion $ConstantsVersion --PatternsVersion $ConstantsVersion \
        --outputNTUP_FTKIPFile OUT.NTUP_FTKIP.root --outputNTUP_FTKFile OUT.NTUP_FTK.root --outputBS_FTKFile OUT.BS_FTK.root \
        --FTKSetupTag $FTKSetupTag --PixelClusteringMode 101 --FixEndCapL0 False \
        --preInclude r2e:./TrigFTK_preinclude.py &
       pid="$!"
       wait
    else
        echo "Running joboptions for ${towers} towers"
        
        write_runargsRAWtoESD
        write_runwrapperRAWtoESD
        write_FTKFullSimulationBank
        write_runwrapper_FTKFullSimulationBank
        write_runargsFTKSimulationMerge
        # write_runwrapper_FTKSimulationMerge
        chmod u+x runwrapper.RAWtoESD.sh
        chmod u+x runwrapper.FTKSimulationMergeFinal.sh
        ./runwrapper.RAWtoESD.sh &
        pid="$!"
        wait
        for (( i = 0; i < ${towers}; i++)); do
        chmod u+x runwrapper.FTKFullSimulationBank0${i}.sh
        ./runwrapper.FTKFullSimulationBank0${i}.sh &
        pid="$!"
        wait
        done
        #./runwrapper.FTKSimulationMergeFinal.sh
    fi


   # echo '#!/bin/bash
   # cd ../xTakashiAlex/source/
   # source P1setup.sh
   # cd ../build
   # source x86_64*/setup.sh
   # cd '$testarea'/run_'$tstamp'
   # MonitoringFTKHWSWComparison -f OUT.BS_FTK.root -n OUT.NTUP_FTK.root -p '$TDAQ_PARTITION''>monitoringftkhwswcomparison.sh

cat <<EOF > monitoringftkhwswcomparison.sh
#!/bin/bash
source /det/ftk/ftk_setup.sh FTK-02-00-09
export TDAQ_PARTITION=FTK-DQM-HWSWComparison
. /sw/atlas/AtlasSetup/scripts/asetup.sh 21.1.7,AthenaP1,here
source /atlas-home/0/britzger/testFTKStandaloneMonitoring/x86_64-slc6-gcc62-opt/setup.sh
/atlas-home/0/britzger/testFTKStandaloneMonitoring/x86_64-slc6-gcc62-opt/bin/MonitoringFTKHWSWComparison -f ./out._0001.data -n OUT.NTUP_FTK.root -v
EOF
#/atlas-home/0/giulini/testdir/build/x86_64-slc6-gcc62-opt/bin/MonitoringFTKHWSWComparison -f OUT.BS_FTK.root -n OUT.NTUP_FTK.root'>monitoringftkhwswcomparison.sh

  chmod u+x monitoringftkhwswcomparison.sh
  ./monitoringftkhwswcomparison.sh &
  #pid of the last started process is needed for cleaning in signal trapping, wait for the children before proceeding
  pid="$!"
  wait


  # cp OUT.NTUP_FTK.root $filearea/NTUP_FTK/OUT.NTUP_FTK_${tstamp}.root
  # cp OUT.BS_FTK.root $filearea/BS_FTK/OUT.BS_FTK_${tstamp}.root
  # cp out.histo.root $filearea/ComparisonHisto/out.histo_${tstamp}.root

  cd ../
  # rm -rf run_$tstamp

done

