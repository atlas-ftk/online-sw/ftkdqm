#ifndef __gnamFTKTrack__
#define __gnamFTKTrack__

// ftkdqm
#include <boost/integer.hpp>
#include <string>
#include <TMath.h>
#include <Eigen/Core>
#define EIGEN_DEVICE_FUNC 
#include "ftkdqm/EigenHalf.h"
#undef EIGEN_DEVICE_FUNC  



using Eigen::half_impl::__half_raw;

namespace daq {
  namespace ftk {
    /*! \brief Class representation of an FTK raw track for gnam
     */
    class gnamFTKTrack{
      public:
        /*! \brief Constructor, does nothing
        */
        gnamFTKTrack();
        /*! \brief Constructor setting track parameters
         */
        gnamFTKTrack(
            uint16_t chiSq_bits, uint16_t d0_bits, uint16_t z0_bits, uint16_t cotTh_bits, uint16_t phi0_bits, uint16_t curv_bits, uint16_t tower,
            uint8_t nPixHits=0, uint8_t nSctHits=0, uint16_t layermap=0);

        /* Get track parameter bits 
        */
        uint16_t GetD0Bits(    void ) const { return m_d0_bits; 		}
        uint16_t GetZ0Bits(    void ) const { return m_z0_bits; 		}
        uint16_t GetCotThBits( void ) const { return m_cotTh_bits;  }
        uint16_t GetPhi0Bits(  void ) const { return m_phi0_bits; 	}
        uint16_t GetChiSqBits( void ) const { return m_chiSq_bits;  }
        uint16_t GetCurvBits(  void ) const { return m_curv_bits; 	}
	uint16_t GetTower() const { return m_tower ;}
        uint16_t GetLayerMap(  void ) const { return m_layermap; }
        bool     GetHitInLayer( int layer ) const;  

   /* Get track parameters */

        uint8_t GetNPixHits(  void ) const {  return m_numb_pix_hits; }
        uint8_t GetNSctHits(  void ) const {  return m_numb_sct_hits; }

        /*  Transform 16 bit track parameter to double
         */


 double GetD0()const {
		uint16_t d0_bits = GetD0Bits(); 
		double m_d0 = 0;
		m_d0 = float(Eigen::half(__half_raw(d0_bits)));
		//double m_d0 = (double)d0_bits / d0_multiplier; 
		return m_d0;}

double GetZ0() const {
		uint16_t z0_bits = GetZ0Bits(); 
		double m_z0 = 0;
		m_z0 = float(Eigen::half(__half_raw(z0_bits)));
		//double m_z0 = (double)z0_bits / z0_multiplier;
		return m_z0; }

double GetCotTh() const {
		uint16_t cotTh_bits = GetCotThBits(); 
		double m_cotTh = 0;
		m_cotTh = float(Eigen::half(__half_raw(cotTh_bits)));
		//double m_cotTh = (double)cotTh_bits / cot_multiplier; 
		return m_cotTh ;}
double GetPhi0() const { 
		uint16_t phi0_bits = GetPhi0Bits();
		double m_phi0 = 0;
		m_phi0 = float(Eigen::half(__half_raw(phi0_bits)));
		//double m_phi0 = (double)phi0_bits / phi_multiplier; 
		return m_phi0;}

double GetChiSq()	const { 
		uint16_t chiSq_bits = GetChiSqBits();
		double m_chiSq = 0;
		m_chiSq = float(Eigen::half(__half_raw(chiSq_bits)));
		//double m_chiSq = ( (double)chiSq_bits ) / chi2_multiplier; 
		return m_chiSq ;}

double GetCurv()	const { 
		uint16_t curv_bits = GetCurvBits();
		double m_curv = 0;
		m_curv = float(Eigen::half(__half_raw(curv_bits)));
		//double m_curv = (double)curv_bits / curv_multiplier; 
		return m_curv;}
//daq::ftk::gnamFTKTrack track;
double GetTheta()  const {
		double m_theta =  TMath::ATan(1./GetCotTh());
		return m_theta ;}

double GetEta() const {
		double m_eta = TMath::ASinH(GetCotTh());
		//double m_eta = -TMath::Log(TMath::Tan(track.GetTheta()/2.));
		return m_eta ;}

 /* Calculate various other track quantities
         */
       // double GetPt(    void ) const { return m_pt; }
        double GetPt() const{
			double temp = 0;
		if (m_curv != 0){
			temp = GetCurv();
			double m_pt =(TMath::Abs(1./(2.*temp)));
			return m_pt;}
		else 
			return (-1);
	}
	double GetInvPt( void ) const { return 1./m_pt; }
      /*  double GetTower() const{
		uint16_t tower = GetTowerBits();
		double m_tower = 0;
		m_tower = float(Eigen::half(__half_raw(tower)));
		//double m_curv = (double)curv_bits / curv_multiplier; 
		return m_tower;
}*/
        /*! \brief Set all track parameters, transform uint16 -> double
        */
        void SetParameters(uint16_t chiSq_bits, uint16_t d0_bits, uint16_t z0_bits, uint16_t cotTh_bits, uint16_t phi0_bits, uint16_t curv_bits, uint16_t tower,
            uint8_t nPixHits=0, uint8_t nSctHits=0, uint16_t layermap=0);


        /*! \brief Print track info into std::string
         *  \param verbosity Defines how much is printed
         */
        std::string Print( int mode=0 ) const;

      private:
        uint16_t m_d0_bits;
        uint16_t m_z0_bits;
        uint16_t m_cotTh_bits;
        uint16_t m_phi0_bits;
        uint16_t m_chiSq_bits;
        uint16_t m_curv_bits;
        uint16_t m_layermap;
       // uint16_t m_tower_bits;
                              // ranges from FTK_DataFormat_v2.4.pdf
        double m_d0;           // ( -32    ... 32   )  [mm]
        double m_z0;           // ( -320   ... 320  )  [mm]
        double m_cotTh;        // ( -6.55  ... 6.55 ) 
        double m_phi0;         // (  0     ... 2Pi  )  [rad]
        double m_curv;         // ( -0.65  ... 0.65 )  [1/2MeV]
        double m_chiSq;        // (  0     ... 655  )
        uint16_t m_tower;
	double m_theta;
	double m_eta;
        double m_pt;
        uint8_t m_numb_pix_hits;
        uint8_t m_numb_sct_hits;

        //TODO: Use these values for uint16 to double conversion later 
        static const int sixteen_bit_offset       = 32767;  // 2**15 -1
        static const int sixteen_bit_max          = 65535;  // 2**16 -1
        static constexpr double d0_multiplier      = 1000.;  // reciprocal of precision 
        static constexpr double z0_multiplier      = 100.;
        static constexpr double cot_multiplier     = 4000.;   
        static constexpr double phi_multiplier     = 10000.;
        static constexpr double curv_multiplier    = 50000.; // TrigFTK_RawData: 2.e7;
        static constexpr double chi2_multiplier    = 1000.;
    };
  } // namespace ftk
} // namespace daq
#endif //__gnamFTKTrack__
