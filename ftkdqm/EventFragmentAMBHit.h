#ifndef EVENTFRAGMENTAMBHIT_H_
#define EVENTFRAGMENTAMBHIT_H_

#include "ftkcommon/EventFragment.h"
#include "ftkdqm/AMBHitObjectEvent.h"

#include <algorithm>

namespace daq {
  namespace ftk {
    
    class EventFragmentAMBHit : public EventFragment
    {      
    public:
      /*! \brief Constructor
       */
      EventFragmentAMBHit();
      
      /*! \brief Destructor
       */
      virtual ~EventFragmentAMBHit();
      
      /*! \brief Get data for the event, not including EE word or L1ID
       *
       * \return event data
       */
      std::vector<unsigned> getData() const;

      /*! \brief Getter of AMBHitObject Event
       */      
      AMBHitObjectEvent getEventHits() const;
      
      /*! \brief Load the event fragment
       *
       * Extacts L1ID and removed trailer. Use getData() to obtain the
       * data from the event frament.
       *
       * \param data event fragment data
       */
      virtual void parseFragment(const std::vector<unsigned>& data);

      /*! \brief Output the bitstream for the event
       *
       * \return The raw data of the spy buffer
       */
      virtual std::vector<unsigned> bitstream() const;
      
      /*! \brief Clear hits associated to instance
       */
      void Reset( void );

      void      setBCID(uint32_t bcid)            { m_bcid = bcid; }
      uint32_t  getBCID( void ) const             { return m_bcid;}   
      void      setRunNumber(uint32_t rn)         { m_runNr = rn;}
      uint32_t  getRunNumber( void ) const        { return m_runNr; }

    private:

      /*! \brief Payload of a spy buffer
       */
      std::vector<unsigned> _data;

      /*! \brief An object to hold hits found in an event
       */
      AMBHitObjectEvent eventHits;
      
      uint32_t m_bcid;
      uint32_t m_runNr;

    };

    /*! \brief Parse raw Hit output spybuffers event-by-event
     *
     * The contents of the last event are ignored, to ensure
     * complete events. Also any data before the first event
     * header is ignored.
     *
     * \param data raw Hit output spybuffer data
     *
     * \return list of EventFragmentAMBHit corresponding
     */

    std::vector<EventFragment*> AMBHit_splitFragments(const std::vector<unsigned>& data);
        
  }
}


#endif // EVENTFRAGMENTAMBHIT_H_
