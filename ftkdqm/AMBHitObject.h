#ifndef AMBHITOBJECT_H_
#define AMBHITOBJECT_H_

#include <vector>

namespace daq {
  namespace ftk {
    
    class AMBHitObject
    {      
    public:
      /*! \brief Constructor
       */
      AMBHitObject();

      /*! \brief Constructor with a full list of arguments
       *  \param raw 32bit word
       *  \param bitmap
       *  \param the number of hits
       *  \param missing layer IDs
       *  \param chip ID
       *  \param link ID
       *  \param LAMB ID
       *  \param pattern address
       */
      AMBHitObject(unsigned data,
		   unsigned SSID);
      
      /*! \brief Copy constructor
       */
      AMBHitObject(const AMBHitObject &obj);

      /*! \brief Destructor
       */
      virtual ~AMBHitObject();

      /*! \brief Getter function of raw 32bit word
       */
      unsigned getData() const;

      /*! \brief Getter function of SSID
       */
      unsigned getSSID() const;
      
      /*! \brief Printing function
       */
      void print();
                  
    private:

      /*! \brief Raw 32bit word
       */
      unsigned data;

      /*! \brief Super strip ID
       */
      unsigned ssID;

    };
        
  }
}


#endif // AMBHitObject_H_
