#ifndef AMBROADOBJECTEVENT_H_
#define AMBROADOBJECTEVENT_H_

#include "ftkdqm/AMBRoadObject.h"

#include <vector>

namespace daq {
  namespace ftk {
    
    class AMBRoadObjectEvent
    {      
    public:
      /*! \brief Constructor
       */
      AMBRoadObjectEvent();

      /*! \brief Constructor with a full list of arguments
       *  \param Level-1 ID
       *  \param the number of roads
       *  \param vector of ROAD objects
       */
      AMBRoadObjectEvent(unsigned l1ID,
			 unsigned nRoad,			 
			 std::vector<AMBRoadObject> roads);
      
      /*! \brief Destructor
       */
      virtual ~AMBRoadObjectEvent();

      /*! \brief Getter function of the number of roads
       */      
      unsigned getNRoad() const;

      /*! \brief Getter function of Level-1 ID
       */
      unsigned getL1ID() const;

      /*! \brief Getter function of the vector of ROAD objects
       */
      std::vector<AMBRoadObject> getRoads() const;

      /*! \brief Set the number of roads
       */
      void setNRoad(unsigned ui);

      /*! \brief Set Level-1 ID
       */
      void setL1ID(unsigned ui);

      /*! \brief Set the vector of ROAD objects
       */
      void setRoads(std::vector<AMBRoadObject> v);

      /*! \brief Push-back a ROAD object in the vector
       */
      void pushRoad(AMBRoadObject aro);
            
    private:

      /*! \brief The number of roads
       */
      unsigned nRoad;

      /*! \brief Level-1 ID
       */
      unsigned l1ID;

      /*! \brief Vector of ROAD objects
       */
      std::vector<AMBRoadObject> roads;
      
    };
        
  }
}


#endif // AMBRoadObjectEvent_H_
