#ifndef __gnamFTKTowerHisto__
#define __gnamFTKTowerHisto__

// ftkdqm
#include <boost/integer.hpp>
#include <string>
#include <vector>
#include <TMath.h>
#include <iostream>

class GnamHisto;

namespace daq {
  namespace ftk {
    /*! \brief Collection of histograms for eta and phi towers.
     *  At the moment overlapping towers are not possible. 
     */
    class gnamFTKTowerHisto{
    public:
      /*! \brief Constructor histograms with fixed bin width
       * \param name        Histogram collection name
       * \param title       Histogram collection title;xAxis title;yAxis title
       * \param nbinsx      Number of bins
       * \param xlow        Lower axis edge
       * \param xup         Upper axis edge
       * \param slices_eta  Definition of eta slices (bin edges)
       * \param slices_phi  Definition of phi slices (bin edges)
       */
  /* gnamFTKTowerHisto(const std::string name, const std::string title,
          Int_t nbinsx, Double_t xlow, Double_t xup,
          std::vector< Double_t > slices_eta=std::vector< Double_t >(),
          std::vector< Double_t > slices_phi=std::vector< Double_t >());*/

      /*! \brief Constructor histograms with variable bin width
       * \param name        Histogram collection name
       * \param title       Histogram collection title;xAxis title;yAxis title
       * \param nbinsx      Number of bins
       * \param xbins       Bin edges, array of size nbinsx+1 with increasing entries
       * \param slices_eta  Definition of eta slices (bin edges)
       * \param slices_phi  Definition of phi slices (bin edges)
       */

     gnamFTKTowerHisto(const std::string name, const std::string title,
	  Int_t nbinsx, Double_t xlow, Double_t xup,
	  std::vector<uint16_t> tower_id = std::vector<uint16_t>());
     
     gnamFTKTowerHisto(const std::string name, const std::string title,
          Int_t nbinsx, Double_t *xbins,
          std::vector< uint16_t > tower_id=std::vector< uint16_t >());

     gnamFTKTowerHisto(const std::string name, const std::string title,
	  Int_t nbinsx, Double_t xlow, Double_t xup,
	  Int_t nbinsy, Double_t ylow, Double_t yup,  
	  std::vector<uint16_t> tower_id = std::vector<uint16_t>());

      ~gnamFTKTowerHisto();

      /*! \brief Fill histogram corresponding to the eta-phi slice containing eta and phi.
       */
      Int_t Fill(Double_t x, uint16_t towerid, Double_t w=1);  

      Int_t Fill(Double_t x, Double_t y, uint16_t towerid, Double_t w=1);
    /*! \brief Get histogram corresponding the the eta-phi slice containing eta and phi.
       */
      GnamHisto* GetHisto(uint16_t towerid);
      
      /*! \brief Add histos to GnamHisto vector of histos to be published
       * \param gnam_histolist Gnam list of histos to be published
       */
      void Register(std::vector<GnamHisto*> *gnam_histolist);

      /*! \brief Prepare histograms (e.g. profiles) before they are published 
       */
      void PrepareForPublish(void);

    private:
      void Initialize( std::vector< uint16_t > tower_id=std::vector< uint16_t >());
         
       
      std::vector< GnamHisto* > m_histos;

   
      //uint16_t towerid;
       std::vector< uint16_t > m_slices_tower;
      Int_t m_n_tower;
   };  
  } // namespace ftk
} // namespace daq
#endif //__gnamFTKTowerHisto__
