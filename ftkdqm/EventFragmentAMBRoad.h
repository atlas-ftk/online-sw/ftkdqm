#ifndef EVENTFRAGMENTAMBROAD_H_
#define EVENTFRAGMENTAMBROAD_H_

#include "ftkcommon/EventFragment.h"
#include "ftkdqm/AMBRoadObjectEvent.h"

#include <algorithm>

namespace daq {
  namespace ftk {
    
    class EventFragmentAMBRoad : public EventFragment
    {      
    public:
      /*! \brief Constructor
       */
      EventFragmentAMBRoad();
      
      /*! \brief Destructor
       */
      virtual ~EventFragmentAMBRoad();
      
      /*! \brief Get data for the event, not including EE word or L1ID
       *
       * \return event data
       */
      std::vector<unsigned> getData() const;

      /*! \brief Getter of AMBRoadObject Event
       */      
      AMBRoadObjectEvent getEventRoads() const;
      
      /*! \brief Load the event fragment
       *
       * Extacts L1ID and removed trailer. Use getData() to obtain the
       * data from the event frament.
       *
       * \param data event fragment data
       */
      virtual void parseFragment(const std::vector<unsigned int>& data);

      /*! \brief Output the bitstream for the event
       *
       * \return The raw data of the spy buffer
       */
      virtual std::vector<unsigned> bitstream() const;
      
      /*! \brief Clear roads associated to instance
       */
      void Reset( void );

      void      setBCID(uint32_t bcid)            { m_bcid = bcid; }
      uint32_t  getBCID( void ) const             { return m_bcid;}   
      void      setRunNumber(uint32_t rn)         { m_runNr = rn;}
      uint32_t  getRunNumber( void ) const        { return m_runNr; }
      
    private:

      /*! \brief Payload of a spy buffer
       */
      std::vector<unsigned> _data;

      /*! \brief An object to hold roads found in an event
       */
      AMBRoadObjectEvent eventRoads;
              
      uint32_t m_bcid;
      uint32_t m_runNr;

    };

    /*! \brief Parse raw Road output spybuffers event-by-event
     *
     * The contents of the last event are ignored, to ensure
     * complete events. Also any data before the first event
     * header is ignored.
     *
     * \param data raw Road output spybuffer data
     *
     * \return list of EventFragmentAMBRoad corresponding
     */

    std::vector<EventFragment*> AMBRoad_splitFragments(const std::vector<unsigned>& data);
        
  }
}


#endif // EVENTFRAGMENTAMBROAD_H_
