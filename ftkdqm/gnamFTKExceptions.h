#ifndef GNAMFTKEXCEPTIONS_H
#define GNAMFTKEXCEPTIONS_H

#include <ers/ers.h>

namespace daq{

  ERS_DECLARE_ISSUE (gnamFTK, 
			  GnamFTKHistoEx,
			  "FKT gnam histograming issue: " << message,
			  ((std::string) message))
  ERS_DECLARE_ISSUE (gnamFTK, 
			  GnamFTKDecodeEx,
			  "FKT gnam decoding issue: " << message,
			  ((std::string) message))

}

#endif 
