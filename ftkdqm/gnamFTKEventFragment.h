#ifndef __gnamFTKEventFragment__
#define __gnamFTKEventFragment__

// std
#include <vector>
#include <map>
#include <string>

// boost
#include <boost/integer.hpp> // uint16_t, etc

// ftkcommon
#include "ftkcommon/EventFragment.h"

// ftkdqm
#include "ftkdqm/gnamFTKTrack.h"

namespace daq {
  namespace ftk {

    /*! \brief EvantFragment derivation used for dqm
    */
    class gnamFTKEventFragment : public EventFragment
    {
      public:
        
        /*! \brief Constructor, does nothing
        */
        gnamFTKEventFragment();
       
        /*! \brief Destructor, does nothing
        */
        virtual ~gnamFTKEventFragment();

        /*! \brief Code for loading data from a spybuffer
         *
         * \param data The raw data corresponding to the event
         */
        virtual void parseFragment(const std::vector<unsigned int>& data);
        
        /*! \brief Code for loading data from a spybuffer
         *
         * \param data Pointer to raw data corresponding to the event
         * \param size Size of data array
         */
        void parseFragment(const uint32_t *data, uint32_t size);

        /*! \brief Retrieve track parameters from raw track data block. Return gnamFTKTrack
         *
         * \param dataBlock FTK track data block
         */
        gnamFTKTrack unpackTrack(const uint32_t *dataBlock);

        /*! \brief Generate the raw data for the spy buffer
         *
         * \return The raw data of the spy buffer
         */
        virtual std::vector<unsigned int> bitstream() const;

        /*! \brief Clear tracks associated to instance
         */
				void Reset( void );

        /*! \brief Print event fragment info into std::string
         */
        std::string Print( void ) const;

        /*! \brief Return pointer to tracks in event
        */
        const std::vector<daq::ftk::gnamFTKTrack> *getTracks( void ) const;

        void      setBCID(uint32_t bcid)            { m_bcid = bcid; }
        uint32_t  getBCID( void ) const             { return m_bcid;}   
        void      setRunNumber(uint32_t rn)         { m_runNr = rn;}
        uint32_t  getRunNumber( void ) const        { return m_runNr; }
        void	  setLumiBlock( uint32_t lb)        {m_lb = lb ;}
        uint32_t  getLumiBlock( void ) const        {return m_lb;}
        uint32_t  getNumTracks( void ) const        { return m_tracks.size(); }
        void      setTrackBlockOffsetStart( uint32_t size) { m_trackBlockOffsetStart=size; }
       void      setTrackBlockOffsetEnd( uint32_t size)   { m_trackBlockOffsetEnd=size ; }

      private:
        std::vector<daq::ftk::gnamFTKTrack> m_tracks;

        uint32_t m_bcid;
        uint32_t m_runNr;
	uint32_t m_lb;

        uint32_t m_trackBlockOffsetStart;     // Nr of words in ROB data block before track blocks start
        uint32_t m_trackBlockOffsetEnd ;       // Nr of words in ROB data block after track blocks end
        const uint32_t m_trackBlockSize = 22; // Size of track blocks, expected by decoder. Set according to FTK Data Format v2.2;


		};

	}	// namespace daq
}	// namespace ftk

#endif // __gnamFTKEventFragment__


