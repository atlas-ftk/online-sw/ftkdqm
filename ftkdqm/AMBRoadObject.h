#ifndef AMBROADOBJECT_H_
#define AMBROADOBJECT_H_

#include <vector>

namespace daq {
  namespace ftk {
    
    class AMBRoadObject
    {      
    public:
      /*! \brief Constructor
       */
      AMBRoadObject();

      /*! \brief Constructor with a full list of arguments
       *  \param raw 32bit word
       *  \param bitmap
       *  \param the number of hits
       *  \param missing layer IDs
       *  \param chip ID
       *  \param link ID
       *  \param LAMB ID
       *  \param pattern address
       */
      AMBRoadObject(unsigned data,
		    unsigned bitMap,
		    unsigned numberOfHits,
		    std::vector<unsigned> missingLayers,
		    unsigned chip,
		    unsigned link,
		    unsigned lamb,
		    unsigned patternAddress);
      
      /*! \brief Copy constructor
       */
      AMBRoadObject(const AMBRoadObject &obj);

      /*! \brief Destructor
       */
      virtual ~AMBRoadObject();

      /*! \brief Getter function of raw 32bit word
       */
      unsigned getData() const;

      /*! \brief Getter function of bitmap
       */
      unsigned getBitMap() const;

      /*! \brief Getter function of the number of hits
       */
      unsigned getNumberOfHits() const;

      /*! \brief Getter function of missing layer IDs
       */
      std::vector<unsigned> getMissingLayers() const;

      /*! \brief Getter function of chip ID
       */
      unsigned getChip() const;

      /*! \brief Getter function of link ID
       */
      unsigned getLink() const;

      /*! \brief Getter function of LAMB ID
       */
      unsigned getLamb() const;

      /*! \brief Getter function of pattern address
       */
      unsigned getPatternAddress() const;

      /*! \brief Getter function of geographical address
       */
      unsigned getGeoAddress() const;
      
      /*! \brief Printing function
       */
      void print();
                  
    private:

      /*! \brief Raw 32bit word
       */
      unsigned data;

      /*! \brief Bitmap
       */
      unsigned bitMap;

      /*! \brief The number of hits in the road
       */
      unsigned numberOfHits;

      /*! \brief Missing layer IDs
       */
      std::vector<unsigned> missingLayers;

      /*! \brief Chip ID
       */
      unsigned chip;

      /*! \brief Link ID
       */
      unsigned link;

      /*! \brief LAMB ID
       */
      unsigned lamb;

      /*! \brief Pattern address
       */
      unsigned patternAddress;

      /*! \brief Geographical address
       */
      unsigned geoAddress;

    };
        
  }
}


#endif // AMBRoadObject_H_
