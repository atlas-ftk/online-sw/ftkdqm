#ifndef AMBHITOBJECTEVENT_H_
#define AMBHITOBJECTEVENT_H_

#include "ftkdqm/AMBHitObject.h"

#include <vector>

namespace daq {
  namespace ftk {
    
    class AMBHitObjectEvent
    {      
    public:
      /*! \brief Constructor
       */
      AMBHitObjectEvent();

      /*! \brief Constructor with a full list of arguments
       *  \param Level-1 ID
       *  \param the number of hits
       *  \param vector of HIT objects
       */
      AMBHitObjectEvent(unsigned l1ID,
			unsigned nHit,			 
			std::vector<AMBHitObject> hits);
      
      /*! \brief Destructor
       */
      virtual ~AMBHitObjectEvent();

      /*! \brief Getter function of the number of hits
       */      
      unsigned getNHit() const;

      /*! \brief Getter function of Level-1 ID
       */
      unsigned getL1ID() const;

      /*! \brief Getter function of the vector of HIT objects
       */
      std::vector<AMBHitObject> getHits() const;

      /*! \brief Set the number of hits
       */
      void setNHit(unsigned ui);

      /*! \brief Set Level-1 ID
       */
      void setL1ID(unsigned ui);

      /*! \brief Set the vector of HIT objects
       */
      void setHits(std::vector<AMBHitObject> v);

      /*! \brief Push-back a HIT object in the vector
       */
      void pushHit(AMBHitObject aro);
            
    private:

      /*! \brief The number of hits
       */
      unsigned nHit;

      /*! \brief Level-1 ID
       */
      unsigned l1ID;

      /*! \brief Vector of HIT objects
       */
      std::vector<AMBHitObject> hits;
      
    };
        
  }
}


#endif // AMBHitObjectEvent_H_
