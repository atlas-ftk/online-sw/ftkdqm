/*
    An extention of oh_ls.cxx originally written by Serguei Kolos
    
    Developed my M. Lisovyi

*/

#include "ftkDataFlowOverview.h"

#include <array>
#include <iostream>
#include <memory>
#include <utility>      // pair
#include <vector>
#include <map>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <cmdl/cmdargs.h>
#include <ipc/core.h>

#include <oh/OHProviderIterator.h>
#include <oh/OHServerIterator.h>
#include <oh/OHIterator.h>
#include <oh/OHRootReceiver.h>
#include <oh/OHRootProvider.h>
#include <oh/OHRawProvider.h>


#include "TROOT.h"
#include "TFile.h"
#include "TKey.h"
#include "TH1.h"
#include "TH2.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TStyle.h"
#include "TLatex.h"
#include "TObjArray.h"
#include "TObjString.h"

template <class Iterator>
std::vector<TH1*> get_histograms( const IPCPartition & partition, 
            const std::string & server_name, 
            const std::string & provider_name,
            const std::vector<TString> desired_histograms)
{
  std::vector<TH1*> saved_histograms;
  try
  {
    OHProviderIterator pit( partition, server_name, provider_name );
    // the map to store the status of all channels for all boards separately for in and out
    //std::map< std::string, std::map<std::string, std::vector<float>> > chStatusMap;

    if( debug ){
      std::cout << " Requesting = "; 
      for (TString h: desired_histograms) std::cout << h << " ";
      std::cout << std::endl;
      std::cout << pit.entries() << " OH providers found on server '" << server_name << "' in partition '" << partition.name() << "':" << std::endl;
    }
    int hitcount = 0;
    while ( pit++ ) {

        if( debug ) std::cout << "    " << pit.name( ) << "\t" << ( pit.isActive() ? "[alive]" : "[absent]" );
        // do not consider outdated providers and empty histo names
        if ( !pit.isActive() || desired_histograms.size() == 0 ) {
          if( debug ) std::cout << std::endl;
          continue;
        }
        
        // get ready to iterate over histos
        Iterator hit( partition, server_name, pit.name(), ".*");
        if( debug )
          std::cout << " contains " << hit.entries() << " histogram(s)" << std::endl;
        if( hit.entries() == 0 ) {
          std::cout << "No histos found." << std::endl;
          continue;
        }

        // iterate over histos
        while ( hit() ){
          if( debug ) std::cout << pit.name()<<" "<<hit.name() << "\t" << hit.time() << std::endl;

          // check if histogram is desired
          TString h_name(hit.name());
          h_name = h_name(0,h_name.Length()-3);
          for (TString h: desired_histograms){
            if (TString(h_name).Contains(h)){
              // get a histogram
              OHRootHistogram ohh = OHRootReceiver::getRootHistogram( partition, server_name, pit.name(), hit.name() );
              TH1* this_histogram = (TH1*)ohh.histogram->Clone();
              TString time_string = Form("time%lu",hit.time().total_mksec_utc());
              this_histogram->SetName(this_histogram->GetName() + TString(Form("_n%d_",hitcount))+time_string);
              saved_histograms.push_back(this_histogram);
              break;
            }
          }
          hitcount++;
       }
    }
  }
  catch( ers::Issue & ex )
  {
      ers::error( ex );
      return {};
  }
  return saved_histograms;  
}




void printColors(int Number, double* Stops,
                               double* Red, double* Green,
                               double* Blue, int NColors) {
     //  For each defined gradient...
     for (int g = 1; g < Number; g++) {
        //  create the colors...
        int nColorsGradient = (int) (floor(NColors*Stops[g]) - floor(NColors*Stops[g-1]));
      std::cout << "nColorsGradient = " << nColorsGradient << "  floor1 = " << floor(NColors*Stops[g]) << "  floor2 = " << floor(NColors*Stops[g-1]) << std::endl;
        for (int c = 0; c < nColorsGradient; c++) {
           std::cout << "R G B = " <<
                   Float_t(Red[g-1]   + c * (Red[g]   - Red[g-1])  / nColorsGradient) << " " <<
                       Float_t(Green[g-1] + c * (Green[g] - Green[g-1])/ nColorsGradient) << " " <<
                       Float_t(Blue[g-1]  + c * (Blue[g]  - Blue[g-1]) / nColorsGradient) << " " << std::endl;
        }
     }
}

int colorInterpolate(int col1, int col2, float w = 0.5){
     TColor*c1 = gROOT->GetColor(col1);
     TColor*c2 = gROOT->GetColor(col2);
     float r = c1->GetRed()  * (1 - w) + c2->GetRed() * w;
     float g = c1->GetGreen() * (1 - w) + c2->GetGreen() * w;
     float b = c1->GetBlue() * (1 - w) + c2->GetBlue() * w;
     float s2 = c1->GetSaturation() * (1 - w) + c2->GetSaturation() * w;
     float h = 0, s = 0, l = 0;
     TColor::RGB2HLS(r, g, b, h, l, s);
     TColor::HLS2RGB(h, l, s2, r, g, b);
     int c = TColor::GetColor(r, g, b);
     return c;
  }

void setPaletteBinary(TH2* h2){
  Int_t colors[] = {kGray, kRed, kGreen}; // #colors >= #levels - 1
  gStyle->SetPalette((sizeof(colors)/sizeof(Int_t)), colors);
  Double_t levels[] = {-1.5, -0.5, 0.5, 1.5};
  h2->SetContour((sizeof(levels)/sizeof(Double_t)), levels);
}
void setPaletteDefault(TH2* h2, const double globalMedian, const bool goToHistMax = false){
  const int nLevels = 50;
  double histMax = h2->GetMaximum();
  
  int levelOffset = 0;
  if (goToHistMax && histMax > globalMedian) levelOffset = 10;

  Int_t colors[nLevels];
  colors[0] = kGray;
  colors[1] = kRed;
  for (int i = 2; i < nLevels-levelOffset; i++) colors[i] = colorInterpolate(kCyan,kOrange,(i-2.)/nLevels);
  
  Double_t levels[nLevels];
  levels[0] = -1.5;
  levels[1] = -0.5;
  levels[2] = 0.5;
  for (int i = 3; i < nLevels-levelOffset; i++) levels[i] = globalMedian*(0.5 + (i-3.)/nLevels);
  
  if (goToHistMax && histMax > globalMedian){
    for (int i =  nLevels-levelOffset; i < nLevels; i++){
      float color_scale = (i-(nLevels-levelOffset))*1.0/levelOffset;
      double level_scale = 0.5 + globalMedian  +(histMax-globalMedian)*color_scale;
      //std::cout << globalMedian << " " << histMax << " " << i << " " << level_scale << " " << color_scale << std::endl;
      colors[i] = colorInterpolate(kOrange,kRed,color_scale );
      if (i+1 < nLevels) levels[i+1] =level_scale;
    }
  }
    
  gStyle->SetPalette((sizeof(colors)/sizeof(Int_t)), colors);
  h2->SetContour((sizeof(levels)/sizeof(Double_t)), levels);
}

void setPaletteBusyFrac(TH2* h2,const int globalMedian){
  Int_t colors[] = {kGray, kGreen, kYellow,
                                   colorInterpolate(kYellow,kOrange),
                                   kOrange,
                                   kRed}; // #colors >= #levels - 1
  gStyle->SetPalette((sizeof(colors)/sizeof(Int_t)), colors);
  //Double_t levels[] = {-1.5, -0.5, 0.001, 0.33, 0.66, 1.0, h2->GetMaximum()};
  Double_t levels[] = {-1.5, -0.5, 0.001, 33, 66, 100, std::max(h2->GetMaximum(),101.)};
  h2->SetContour((sizeof(levels)/sizeof(Double_t)), levels);
}

void setPaletteTime(TH2* h2,const double globalMedian, const double globalMax, const double globalMin){
  if(debug){
    std:: cout << h2->GetMinimum() << " - "<<  h2->GetMaximum()<<std::endl;
    std::cout << "Using time palette with param " << globalMedian << std::endl;
  }
  const int nLevels = 50;
  Int_t colors[nLevels];
  colors[0] = kGray;
  colors[1] = kRed;
  for (int i = 2; i < nLevels; i++) colors[i] = colorInterpolate(kCyan,kOrange,(i-2.)/nLevels);
  gStyle->SetPalette((sizeof(colors)/sizeof(Int_t)), colors);
  Double_t levels[nLevels];
  levels[0] = -1.5;
  levels[1] = -0.5;
  levels[2] = 0.5;
  for (int i = 3; i < nLevels; i++) levels[i] = globalMedian*(0.5 + (i-3.)/nLevels);
  h2->SetContour((sizeof(levels)/sizeof(Double_t)), levels);
}

void setPaletteL1ID(TH2* h2,const int globalMedian){
  Int_t colors[] = {kViolet,kGray, kRed, kOrange, colorInterpolate(kGreen,kCyan,0.7), kGreen, colorInterpolate(kGreen,kYellow,0.7), kOrange}; // #colors >= #levels - 1
  gStyle->SetPalette((sizeof(colors)/sizeof(Int_t)), colors);
  Double_t levels[] = {-std::numeric_limits<double>::max(),-1.5, -0.5, 0.5, globalMedian*0.5, globalMedian-0.5, globalMedian+0.5, globalMedian*1.5, std::max(h2->GetMaximum(),globalMedian*2.)};
  h2->SetContour((sizeof(levels)/sizeof(Double_t)), levels);
}

void setPaletteGrey(){
  const int ncol = 2;
  std::array<double, ncol> Ends, Red, Green, Blue;
  Ends  = { 0.0, 1.0};
  Red   = { 0.7, 0.7};
  Green = { 0.7, 0.7};
  Blue  = { 0.7, 0.7};
  TColor::CreateGradientColorTable(ncol,Ends.data(),Red.data(),Green.data(),Blue.data(),ncol-1);
  gStyle->SetNumberContours(ncol-1);
}

void setPaletteColours(const std::string& hName) {
  const int ncol = 5;
  std::array<double, ncol> Ends, Red, Green, Blue;
  if(       hName.find("Link") != std::string::npos && 
      hName.find("Status") != std::string::npos ) {
    Ends  = { 0.0, 0.25, 0.5, 0.75, 1.0};
    Red   = { 1.0, 1.0, 0.0, 1.0, 1.0};
    Green = { 1.0, 0.0, 1.0, 1.0, 1.0};
    Blue  = { 1.0, 0.0, 0.0, 0.0, 0.0};
  } else if(       hName.find("Fifo") != std::string::npos && 
            hName.find("Busy") != std::string::npos && 
            hName.find("Fraction") == std::string::npos ) {
    Ends  = { 0.0, 0.25, 0.5, 0.75, 1.0};
    Red   = { 1.0, 0.0, 1.0, 1.0, 1.0};
    Green = { 1.0, 1.0, 0.0, 0.0, 0.0};
    Blue  = { 1.0, 0.0, 0.0, 0.0, 0.0};
  } /*else if(       hName.find("Fifo") != std::string::npos && 
            hName.find("Empty") != std::string::npos && 
            hName.find("Fraction") == std::string::npos ) {
    Ends  = { 0.0, 0.25, 0.5, 0.75, 1.0};
    Red   = { 1.0, 0.0, 1.0, 1.0, 1.0};
    Green = { 1.0, 1.0, 0.0, 0.0, 0.0};
    Blue  = { 1.0, 0.0, 0.0, 0.0, 0.0};
  } else if(       hName.find("Fifo") != std::string::npos && 
            hName.find("Busy") != std::string::npos && 
            hName.find("Fraction") != std::string::npos ) {
    Ends  = { 0.0, 0.25, 0.75, 0.9, 1.0};
    Red   = { 1.0, 0.0, 1.0, 1.0, 1.0};
    Green = { 1.0, 1.0, 1.0, 0.0, 0.0};
    Blue  = { 1.0, 0.0, 0.0, 0.0, 0.0};
  } else if(       hName.find("Fifo") != std::string::npos && 
            hName.find("Empty") != std::string::npos && 
            hName.find("Fraction") != std::string::npos ) {
    Ends  = { 0.0, 0.25, 0.75, 0.9, 1.0};
    Red   = { 1.0, 0.0, 1.0, 1.0, 1.0};
    Green = { 1.0, 1.0, 1.0, 1.0, 1.0};
    Blue  = { 1.0, 0.0, 0.0, 0.0, 0.0};
  }*/


  TColor::CreateGradientColorTable(ncol,Ends.data(),Red.data(),Green.data(),Blue.data(),ncol-1);
  if( debug )
    printColors(ncol,Ends.data(),Red.data(),Green.data(),Blue.data(),ncol-1);
  gStyle->SetNumberContours(ncol-1);

}

void plotStatus(std::map<std::string, TH2F>& h2Map, const std::string& board_name,  const std::string& hName)
{
  std::cout << "drawing histmap" << std::endl;
  gStyle->SetOptStat(0);
  // set up a palette
  setPaletteColours(hName);

  // ticks on the right and on top
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  // print also zero bins
  gStyle->SetHistMinimumZero();
  // set the text format for the fraction information
  if( hName.find("Fraction") != std::string::npos )
    gStyle->SetPaintTextFormat(".1f");

  int nIn      = h2Map["In"].GetEntries();
  int nOut      = h2Map["Out"].GetEntries();
  int nExtra      = h2Map["Extra"].GetEntries();
  if( debug )
    std::cout << "hIn entries = " << nIn << "    hOut entries = " << nOut << "    hExtra entries = " << nExtra << std::endl;
  if( (nIn != 0 || nOut != 0) && nExtra != 0)
    std::cout << "WARNING: There have been found monitoring data for In/Out channels and something else. This should not happen..." << std::endl;

  // number of histograms to be plotted
  int nNonZeroHistos = 0;
  for( auto const &h2PairKey: h2Map ){
    if( h2PairKey.second.GetEntries() != 0 )
      nNonZeroHistos++;
  }

  // do plotting in a canvas with nNonZeroHistos pads
  TCanvas c("c1", "c", 1200,800);
  c.Divide(nNonZeroHistos,1);
  int i = 0;
  for( auto &h2PairKey: h2Map ){
    if( h2PairKey.second.GetEntries() != 0 ) {
      i++;
      c.cd(i);
      plotStatusTH2( h2PairKey.second, hName );
    }      
  }

  //  save the ouput as png and pdf files
  c.SaveAs( (board_name + "_" + hName + ".png").c_str() );
  c.SaveAs( (board_name + "_" + hName + ".pdf").c_str() );

}

void plotStatusTH2(TH2F& h2, const std::string& hName)
{

  gPad->SetGridx();
  gPad->SetGridy();
  gPad->SetTopMargin(0.06);
  gPad->SetBottomMargin(0.05);
  gPad->SetLeftMargin(0.15);
  gPad->SetRightMargin(0.1);// 0.02
  h2.GetXaxis()->SetTitleOffset(0.75);
  // alphabetic sort of labels on Y axis
  h2.LabelsOption("a","Y");  
  // vertical writing of labels on X axis
  h2.LabelsOption("v","X");  
  // strip off bins that didn't get entries. This is not needed, in general
  h2.LabelsDeflate("Y");
  //  not needed? we do not plot the paletter so far
  // h2.GetZaxis()->SetNdivisions(3,false);
  if( hName.find("Fraction") != std::string::npos )
    h2.GetZaxis()->SetRangeUser(-1,100);
  else
    h2.GetZaxis()->SetRangeUser(-1,2);
  h2.Draw("colz text");
}

void harmonizeRanges(const std::vector<TH2*> histograms){
  int max = -1;
  int min = -1; 
  for (TH2*h:histograms){
    int hmax = floor(h->GetMaximum());
    int hmin = floor(h->GetMinimum());
    if (hmax > max) max = hmax;
    if (hmin < min) min = hmin;
  }
  for (TH2*h:histograms){
    h->SetMaximum(max);
    h->SetMinimum(min);
  }
}

TH2F* cellularize(TH1*h_in, float rescale = 1.0){
  TString name = h_in->GetName();
  TString hist_board_name = name(name.Index(".")+1,name.Index("_common") - name.Index(".")-1);
  int nbinsX = h_in->GetNbinsX();
  TH2F * h2_out = new TH2F( name.ReplaceAll("1d","2d"), "", nbinsX, 0,nbinsX, 1, 0, 1 );
  for (int i = 0; i <= nbinsX; i++){
    h2_out->SetBinContent(i,1,h_in->GetBinContent(i)*rescale);
  }
  h2_out->GetYaxis()->SetTitle(hist_board_name);

  // remove y axis ticks
  h2_out->GetYaxis()->SetLabelOffset(999);
  h2_out->GetYaxis()->SetLabelSize(0);
  
  h2_out->GetZaxis()->SetLabelSize(0.1);
  h2_out->GetXaxis()->SetLabelSize(0.1);
  h2_out->GetXaxis()->SetLabelOffset(0.02);
  
  h2_out->GetYaxis()->SetTitleSize(0.1);
  h2_out->GetYaxis()->SetTitleOffset(0.2);
  h2_out->GetYaxis()->CenterTitle();
  h2_out->GetYaxis()->SetTickLength(0);
  h2_out->GetXaxis()->SetTickLength(0.5);
  h2_out->GetXaxis()->SetNdivisions(nbinsX,false);
  
  h2_out->SetMarkerSize(5); // determines size of cell text

  h2_out->SetMinimum(-1);
  return h2_out;
}

TH2F* cellularize(std::vector<TH1*>hvec, float rescale = 1.0){

  TH1*h_in = hvec[0];
  TString name = h_in->GetName();
  TString hist_board_name = name(name.Index(".")+1,name.Index("_common") - name.Index(".")-1);
  int nbinsX = h_in->GetNbinsX();
  int nbinsY = hvec.size();

  TH2F * h2_out = new TH2F( name.ReplaceAll("1d","2d"), "", nbinsX, 0,nbinsX, nbinsY, 0, nbinsY);

  for (int i = 0; i <= nbinsX; i++){
    for (int j = 0; j < nbinsY; j++){
      if (debug) std::cout << name << " " << i << " " << j+1 << " " << hvec[j]->GetBinContent(i)*rescale << std::endl;
      h2_out->SetBinContent(i,j+1,hvec[j]->GetBinContent(i)*rescale);
    }
  }

  for (int i=1;i<=nbinsY;i++){
    TString board_label = hvec[i-1]->GetName();
    board_label = board_label(board_label.Index(".")+1,board_label.Index("_common") - board_label.Index(".")-1);
    h2_out->GetYaxis()->SetBinLabel(i,board_label);
  }

  h2_out->GetYaxis()->SetTickLength(1.0);
  h2_out->GetYaxis()->SetNdivisions(nbinsY,false);
  h2_out->GetXaxis()->SetTickLength(0.5);
  h2_out->GetXaxis()->SetNdivisions(nbinsX,false);
  
  h2_out->SetMarkerSize(1); // determines size of cell text

  h2_out->SetMinimum(-1);
  return h2_out;
}

void label(TH2* h2,const std::vector<TString> labels){
  int nx = h2->GetNbinsX();
  for (int i=1;i<=nx;i++) h2->GetXaxis()->SetBinLabel(i,labels[i-1]);
}

std::vector <TH2*> processHistograms(const std::vector<TH1*> histograms, const TString variable, const bool plot_time){
  std::vector<TH2*> histograms_filtered;
  std::vector<TString> histogram_names;

  std::map<TString, std::vector<TH1*>> histograms_filtered_byboard;
  const std::vector<TString> boards = {"IM", "DF", "AUX", "AMB", "SSB"};
  
  //sort histograms
  for (TH1* h: histograms){
    if (!h || h->GetNbinsX() == 0) continue;
    TString name = h->GetName();
    if (!name.Contains(variable) || !name.Contains("common") || !name.Contains("FTK")) continue;
    
    TString subname  = name(0,name.Index(":"));
    bool repeatHist = false;
    for (TString n: histogram_names) if (n==subname) repeatHist = true;
    if (repeatHist){
      if (debug) std::cout << "Duplicate histograms found for " << subname << std::endl;
      continue;
    }
    histogram_names.push_back(subname);

    TString metadata = name(name.Index(":")+1,name.Length());
    if (metadata.Contains("_")){
      TObjArray* tokens = metadata.Tokenize("_");
      if(plot_time && tokens->GetSize() < 2){
        std::cout << "No time information found." << std::endl;
        return histograms_filtered;
      }else if (plot_time){
        TString h_time  = ((TObjString*) tokens->At(2))->GetString().Data();
        h->Rebin(h->GetNbinsX());
        h->SetBinContent(1,h_time.Atoll());
      }
    }
    
    for (TString b: boards){
      if (name.Contains(b)){
        histograms_filtered_byboard[b].push_back(h);
        break;
      }
    }
  }

  // format histograms
  for (TString b: boards){
    if (histograms_filtered_byboard.count(b) == 0 || histograms_filtered_byboard[b].size() == 0){
      if (debug) std::cout << "Board " << b << " missing histograms for " << variable << std::endl;
      continue;
    }
    TH1*h = histograms_filtered_byboard[b][0];
    TString name = h->GetName();
    float rescale_factor = 1.0;
    if (name.Contains("AUX") && variable.Contains("EventRate")) rescale_factor = 0.01;
    if (variable.Contains("Fraction")) rescale_factor = 100.;
    if (plot_time) rescale_factor = 1.0;
    //TH2* h2 = cellularize(h, rescale_factor);
    TH2* h2;
    if (histograms_filtered_byboard[b].size() == 1)
      h2 = cellularize(histograms_filtered_byboard[b][0], rescale_factor);
    else
      h2 = cellularize(histograms_filtered_byboard[b], rescale_factor);

    if      (name.Contains("AUX") && variable.Contains("Out")) label(h2,labels_AUX_out);
    else if (name.Contains("AUX"))                             label(h2,labels_AUX_in);
    else if (name.Contains("AMB") && variable.Contains("Out")) label(h2,labels_AMB_out);
    else if (name.Contains("AMB") && variable.Contains("In"))  label(h2,labels_AMB_in);
    else if (name.Contains("SSB") && variable.Contains("In"))  label(h2,labels_SSB_in);
    else for (int i=1;i<=h2->GetNbinsX();i++)
      h2->GetXaxis()->SetBinLabel(i,TString(std::to_string(i-1)));
    histograms_filtered.push_back(h2);
    
  }

  harmonizeRanges(histograms_filtered);
  return histograms_filtered;
}

bool generateImage(std::vector<TH2*> histograms_filtered, const TString variable, const TString outdir, const bool plot_time){
  
  int nBoards = histograms_filtered.size();
  
  // calculate stat quantities used to define color palettes
  std::vector<double> vals;
  double globalMaxVal = std::numeric_limits<double>::min();
  double globalMinVal = std::numeric_limits<double>::max();
  for (TH2*h2:histograms_filtered){
    if (!h2 || h2->GetNbinsX() == 0) continue;
    for (int i = 1; i <= h2->GetNbinsX(); i++){
      double binVal = h2->GetBinContent(i,1);
      if (binVal > 0) vals.push_back(binVal);
      if (binVal > globalMaxVal) globalMaxVal = binVal;
      if (binVal < globalMinVal) globalMinVal = binVal;
    }
  }
  double globalMedianVal = calcMedian(vals);
 
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  gStyle->SetHistMinimumZero();
  TCanvas c("c1", "c1", 600,800);
  gStyle->SetPadTopMargin(0.1);
  c.Divide(1,nBoards,0.002,0.002,0);

  float defaultMarkerTextSize = nBoards*1.1;

  for (int i = 0; i < nBoards; i++){

    c.cd(i+1);
    gPad->SetTopMargin(0.2);
    gPad->SetBottomMargin(0.15);
    gPad->SetRightMargin(-0.2);
    gPad->SetLeftMargin(0.15);
    
    TH2* h2 = histograms_filtered[i];

    // get hist stats for formatting
    float maxVal = -1;
    bool hasFloats = false;
    for (int i = 1; i <= h2->GetNbinsX(); i++){
      float binVal = h2->GetBinContent(i,1);
      if (maxVal < binVal) maxVal = binVal;
      if (int(binVal) != binVal) hasFloats = true;
    }

    // default formatting
    setPaletteDefault(h2, globalMedianVal);
    h2->SetMarkerSize(defaultMarkerTextSize);
    gStyle->SetPaintTextFormat("g");
    bool rotateText = maxVal > 1000 || hasFloats;
    
    // specific formatting
    if (plot_time){
      gStyle->SetPaintTextFormat(".0f");
      setPaletteTime(h2, globalMedianVal,globalMaxVal,globalMinVal);
      rotateText = false;
      h2->GetXaxis()->SetBinLabel(1,"Publish time");
    }
    else if (variable.Contains("EventRate")){
      gStyle->SetPaintTextFormat(".0f");
    }
    else if (variable.Contains("L1id")){
      gStyle->SetPaintTextFormat(".0f");
      float L1idMarkerScale = 0.7;
      if (h2->GetNbinsY() > 2) L1idMarkerScale = 0.5;
      h2->SetMarkerSize(L1idMarkerScale*defaultMarkerTextSize);
      setPaletteDefault(h2, globalMedianVal,true);
      rotateText = true;
    }
    else if (variable.Contains("Link")) setPaletteBinary(h2);
    else if (variable.Contains("BusyFraction")){
      gStyle->SetPaintTextFormat(".0f");
      setPaletteBusyFrac(h2,globalMedianVal);
      rotateText = false;
    }

    if ( rotateText)
      h2->DrawClone("col text89");
    else
      h2->DrawClone("col text");
 
  }
  
  c.cd();
  
  // write title
  TLatex latex;
  latex.SetNDC();
  latex.SetTextSize(0.035);
  latex.SetTextAlign(12);
  latex.SetTextFont(42);
  latex.DrawLatex(0.45,0.99,variable);

  TString outnamebase = outdir+"/ftkDataFlowOverview_"+variable + (plot_time? "_time" : "");
    
  c.SaveAs(outnamebase+".png");
  c.SaveAs(outnamebase+".pdf");//*/
  
  return true;
}

bool writeHistsToOH(const IPCPartition & partition, const std::string & OHServer, std::vector<TH2*> histograms_filtered){
  
  std::unique_ptr<OHRootProvider>  ohProvider;    ///< Histogram provider
  std::shared_ptr<OHRawProvider<>> ohRawProvider;    ///< Raw histogram provider
  try{
    std::string OHName("FTK_DataFlowHistograms"); 
    ohProvider = std::make_unique<OHRootProvider> ( partition , OHServer, OHName );
    ohRawProvider = std::make_unique<OHRawProvider<> >( partition , OHServer,OHName );
  }
  catch(ers::Issue & ex)
  {
    ers::error(ex);
    return false;
  }
    
  for (TH2* h: histograms_filtered){
    try { ohProvider->publish( *h, h->GetName(), true ); }
    catch (ers::Issue & ex){
      ers::error(ex);
    }  
  }
  
  return true;
}

int main( int argc, char ** argv )
{
  try {
    IPCCore::init( argc, argv );
  }
  catch( daq::ipc::Exception & ex ) {
    ers::fatal( ex );
    return 1;
  }

  std::cout << "testing ftkDataFlowOverview" << std::endl;

  CmdArgStr partition_name ( 'p', "partition", "partition_name", "Partition name [default: ATLAS]" );
  CmdArgStr server_name    ( 's', "server",    "server_name", "OH (IS) server name [default: Histogramming]" );
  CmdArgStr provider_name  ( 'n', "provider", "provider_name", "Provider name [default: FTK_.*.*_common]" );
  CmdArgSet debugMode      ( 'd', "debug",     "Debug printouts [default: Reduced verbosity]" );
  CmdArgSet do_publish     ( 'P', "publish",   "Publish processed histograms to OH [default: none]" );
  CmdArgStr save_hists     ( 'o', "output",    "save_hists","Save histograms from OH [default: none]" );
  CmdArgStr outdir         ( 'O', "outdir",    "output_directory","Directory for program output [default: .]" );
  CmdArgStr test_file      ( 't', "test",      "test_file", "Test provided inputs [default: none]" );
  CmdArgSet plot_time      ( 'T', "time",     "Plot times of published histograms in 'total number of microseconds since epoch in UTC' [default: none]" );
                                                       
  CmdLine cmd( *argv, &partition_name, &server_name, &provider_name, &debugMode,&do_publish, &save_hists, &outdir, &test_file, &plot_time, NULL );
  CmdArgvIter arg_iter( --argc, ++argv );
  cmd.description( "Display FTK DataFlow overview per board" );
  
  // set default values
  partition_name = "ATLAS";
  server_name    = "Histogramming";
  provider_name  = "FTK_.*.*_common";
  test_file      = "";
  save_hists     = "";
  outdir         = ".";
  
  // parse command-line agrs
  cmd.parse(arg_iter);
  debug = debugMode;
  std::string testFileStr = std::string(test_file);
  std::string saveHistStr = std::string(save_hists);
  IPCPartition partition = IPCPartition( std::string(partition_name) );

  struct stat st = {0};
  if (stat(outdir, &st) == -1) {
    mkdir(outdir, 0700);
  }
  
    // histograms to retrieve
  const std::vector<TString> histograms_to_read  = {"Busy",
                                                   "EventRate","L1id",
                                                   "LinkInStatus","LinkOutStatus"};

  // histograms to retrieve
  const std::vector<TString> desired_histograms = {"FifoInBusyFraction","FifoOutBusyFraction",
                                                   "EventRate","L1id",
                                                   "LinkInStatus","LinkOutStatus"};
  std::vector <TH1*> fetched_histograms; 
  
  // read in histograms from a test file
  if (testFileStr.size() > 0){
    if(debug) std::cout << "Reading histograms from " << testFileStr << std::endl;
    TFile f(TString(testFileStr),"READ");

    // get all Histogramming histograms written to the file
    TIter nextkey(f.GetListOfKeys());
    TKey *key;
    while ((key = (TKey*)nextkey())) {
      TString name = key->GetName();
      if (TString(key->GetClassName()).Contains("TH")  && name.Contains("Histogramming")){
        for (TString desired_hist:desired_histograms){
          if (name.Contains(desired_hist)){
            TH1* h = (TH1*)f.Get(name);
            if (!h) break;
            h->SetDirectory(0);
            if(debug) std::cout << "Found histogram " << name << std::endl;
            fetched_histograms.push_back(h);
            break;
          }
        }
      }
    }
    f.Close();
  }
  
  // read histograms from partition
  else{
    fetched_histograms = get_histograms<OHHistogramIterator>( partition, std::string(server_name), std::string(provider_name), desired_histograms);
    if (debug){
      std::cout << "Retrieved " << fetched_histograms.size() << " histograms:" << std::endl;
      for (TH1* h: fetched_histograms)  std::cout << h->GetName() << std::endl;
    }
  }

  // save fetched histograms in file
  if (saveHistStr.size() > 0){
    TFile f(outdir + "/" + TString(saveHistStr),"RECREATE");
    for (TH1*h:fetched_histograms) h->Write();  
    f.Close();
  }
  
  std::vector <TH2*> processed_histograms; 
  
  //process histograms & generate ftkDataFlowOverview plots
  if (fetched_histograms.size() > 0){
    for (TString variable: desired_histograms){

      std::vector<TH2*> processed_histograms = processHistograms(fetched_histograms, variable, plot_time);
      if (processed_histograms.size() == 0) continue;
      
      if(debug) std::cout << "Making plots for " << variable << std::endl;
      generateImage(processed_histograms,variable, TString(outdir), plot_time);
      if(do_publish) writeHistsToOH(partition, std::string(server_name),processed_histograms);
    }
  }
  else std::cout << "No histograms retrieved." << std::endl;
  
}
