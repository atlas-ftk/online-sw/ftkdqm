#ifndef ftkDataFlowOverview_H
#define ftkDataFlowOverview_H


#include <iostream>
#include <fstream>
#include <memory>
#include <utility>      // pair
#include <vector>
#include <map>
#include <algorithm>      // max

#include <ipc/core.h>

#include <oh/OHProviderIterator.h>
#include <oh/OHServerIterator.h>
#include <oh/OHIterator.h>
#include <oh/OHRootReceiver.h>

#include "TH2.h"

// do debug printouts
bool debug = false;

const std::vector<TString> labels_AMB_in = {"PIXEL-0","PIXEL-1","PIXEL-2","PIXEL-3","PIXEL-4","PIXEL-5","PIXEL-6","PIXEL-7","SCT-0","SCT-1","SCT-2","SCT-3"};
const std::vector<TString>labels_AMB_out = {"LAMB0_0","LAMB0_1","LAMB0_2","LAMB0_3",
                                            "LAMB1_0","LAMB1_1","LAMB1_2","LAMB1_3",
                                            "LAMB2_0","LAMB2_1","LAMB2_2","LAMB2_3",
                                            "LAMB3_0","LAMB3_1","LAMB3_2","LAMB3_3"};

const std::vector<TString> labels_AUX_in = {"I1 QSFP 0", "I1 QSFP 1", "I1 QSFP 2", "I1 QSFP 3",
                                            "I2 QSFP 0", "I2 QSFP 1", "I2 QSFP 2", "I2 QSFP 3",
                                            "P1 Roads 0","P1 Roads 1","P1 Roads 2","P1 Roads 3",
                                            "P2 Roads 0","P2 Roads 1","P2 Roads 2","P2 Roads 3",
                                            "P3 Roads 0","P3 Roads 1","P3 Roads 2","P3 Roads 3",
                                            "P4 Roads 0","P4 Roads 1","P4 Roads 2","P4 Roads 3",};
const std::vector<TString> labels_AUX_out = {"I1 SSDATA 0","I1 SSDATA 1","I1 SSDATA 2","I1 SSDATA 3","I1 SSDATA 4","I1 SSDATA 5","I1 SSDATA 6","I1 SSDATA 7",
                                             "I2 SSDATA 0","I2 SSDATA 1","I2 SSDATA 2","I2 SSDATA 3", "I2 SFP 0"};

const std::vector<TString> labels_SSB_in = {"I1 QSFP 0","I1_QSFP 1","I1_QSFP 2","I1_QSFP 3"};
  
void plotStatus(std::map<std::string, TH2F>& h2Map, const std::string& board_name = "YYY",  const std::string& hName = "LinkStatus");
void plotStatusTH2(TH2F& h2, const std::string& hName = "LinkStatus");
void generateImage(const std::vector<TH1*> histograms,  const TString variable = "LinkStatus");
void harmonizeRanges(const std::vector<TH2*> histograms);

double calcMedian(std::vector<double> input)
{
  double median;
  size_t size = input.size();
  if (size == 0) return 0;
  sort(input.begin(), input.end());

  if (size  % 2 == 0) median = (input[size / 2 - 1] + input[size / 2]) / 2;
  else median = input[size / 2];
  
  return median;
}

template <class Iterator>
int show_objects(      const IPCPartition & partition, 
            const std::string & server_name, 
            const std::string & provider_name,
            const std::string & histo_name,
            const std::string & type_name = "H",
            const std::string & board_name = "YYY" )
{
    try
    {
      OHProviderIterator pit( partition, server_name, provider_name );
      // the map to store the status of all channels for all boards separately for in and out
      std::map< std::string, std::map<std::string, std::vector<float>> > chStatusMap;
      // the max number of channels in monitoring across all boards
      uint nChMax = 0;
      uint nIChMax = 0;  //  input
      uint nOChMax = 0;  //  output
      uint nEChMax = 0;  //  extra
      // histo name
      std::string hName = histo_name;
      while( hName.find(".*") != std::string::npos )
        hName.erase( hName.find(".*"), 2 );
      if( debug ){
        std::cout << " hName = " << hName << std::endl;
        std::cout << pit.entries() << " OH providers found on server '" << server_name << "' in partition '" << partition.name() << "':" << std::endl;
      }
      while ( pit++ ) {
          if( debug )
            std::cout << "    " << pit.name( ) << "\t" << ( pit.isActive() ? "[alive]" : "[absent]" );
          // do not consider outdated providers and empty histo names
            if ( !pit.isActive() || histo_name.empty() ) {
            if( debug )
                  std::cout << std::endl;
            continue;
          }
          
          // get ready to iterate over histos
          Iterator hit( partition, server_name, pit.name(), histo_name+":1d" );
          if( debug )
            std::cout << " contains " << hit.entries() << " " << type_name << "(s)" << std::endl;
          if( hit.entries() == 0 ) {
            std::cout << "No histos found. Requested histoname = " << histo_name << std::endl;
            continue;
          }

          // iterate over histos
          std::vector<float> inV, outV, extraV;
          while ( hit() )
          {
            if( debug )
            std::cout << "\t" << hit.name() << "\t" << hit.time() << std::endl;
            // get a histogram
            OHRootHistogram ohh = OHRootReceiver::getRootHistogram( partition, server_name,
                                            pit.name(),
                                            hit.name() );
            bool defaultHisto = (ohh.histogram->GetNbinsX() == 1 && ohh.histogram->GetBinContent(1) == -1);
            // fill status of input channels
            if( hit.name().find("In") != std::string::npos && !defaultHisto )
                  for(int i = 1; i <= ohh.histogram->GetNbinsX(); i++)
                        inV.push_back( ohh.histogram->GetBinContent(i) );
            // fill status of output channels
            else if( hit.name().find("Out") != std::string::npos && !defaultHisto )
                  for(int i = 1; i <= ohh.histogram->GetNbinsX(); i++)
                        outV.push_back( ohh.histogram->GetBinContent(i) );
            // otherwise fill status as extra
            else if( !defaultHisto )
                  for(int i = 1; i <= ohh.histogram->GetNbinsX(); i++)
                        extraV.push_back( ohh.histogram->GetBinContent(i) );

          }
          // if any useful information was found, fill the map with statuses
          if (inV.size() != 0 || outV.size() != 0 || extraV.size() != 0)
            chStatusMap[pit.name()] = {      {"In", inV}, 
                                    {"Out", outV}, 
                                    {"Extra", extraV} };
          // check if we get more channels than for previous boards
          if( inV.size() > nIChMax)
            nIChMax = std::min(uint(inV.size()), uint(50));
          if( outV.size() > nOChMax )
            nOChMax = std::min(uint(outV.size()), uint(50));
          if( extraV.size() > nEChMax )
            nEChMax = std::min(uint(extraV.size()), uint(50));
      }
      nChMax = nIChMax + nOChMax;

      // create a TH2 if at least 1 board publishes some status information
      if( chStatusMap.size() != 0 ) {
            if( debug )
                  std::cout << "\tMap contains " <<chStatusMap.size() << " boards; maximal number of channels (nChMax) = " << nChMax << std::endl;            
            std::string hNameFull = board_name + "_" + hName;
            TH2F h2In ( (hNameFull + "In").c_str(), (board_name + " " + hName + "In; Channels").c_str(), 
                        std::max(nIChMax, uint(1)), 0, std::max(nIChMax, uint(1)), 
                        chStatusMap.size(), 0, chStatusMap.size() );
            TH2F h2Out( (hNameFull + "Out").c_str(), (board_name + " " + hName + "Out; Channels").c_str(), 
                        std::max(nOChMax, uint(1)), 0, std::max(nOChMax, uint(1)), 
                        chStatusMap.size(), 0, chStatusMap.size() );
            TH2F h2Extra( (hNameFull + "Extra").c_str(), (board_name + " " + hName + "Extra; Channels").c_str(), 
                        std::max(nEChMax, uint(1)), 0, std::max(nEChMax, uint(1)), 
                        chStatusMap.size(), 0, chStatusMap.size() );
            
            //int iy = 1;
            for( auto const &chStatusPairKey: chStatusMap ){
                  auto chStatusInOutExtraMap = chStatusPairKey.second;
                  auto yBinName = chStatusPairKey.first;
                  yBinName.erase( yBinName.find("_common"), 7 );
                  yBinName.erase( yBinName.find("FTK_"), 4 );
                  uint nInLinks  = chStatusInOutExtraMap.at("In").size();
                  uint nOutLinks = chStatusInOutExtraMap.at("Out").size();
                  // fill input channels
                  for( uint ix = 0; ix < nIChMax; ix++ ) {      
                        h2In.Fill (std::to_string(ix).c_str(),      yBinName.c_str(), (ix < nInLinks)  ? chStatusInOutExtraMap.at("In").at(ix) : -2 );}
                  // fill output channels
                  for( uint ix = 0; ix < nOChMax; ix++ ) {
                        h2Out.Fill(std::to_string(ix).c_str(),      yBinName.c_str(), (ix < nOutLinks) ? chStatusInOutExtraMap.at("Out").at(ix) : -2);}
            }

            // Create input for plotting
            std::map<std::string, TH2F> h2Map = {      {"In", h2In},
                                          {"Out", h2Out},
                                          {"Extra", h2Extra}, };

            plotStatus(h2Map, board_name, hName);
            
      }
    }
    catch( ers::Issue & ex )
    {
          ers::error( ex );
        return 1;
    }
    return 0;
}
/*
template <class Iterator>
std::vector<TH1*> get_histograms( const IPCPartition & partition, 
            const std::string & server_name, 
            const std::string & provider_name,
            const std::vector<TString> desired_histograms)
{
	std::vector<TH1*> saved_histograms;
    try
    {
      OHProviderIterator pit( partition, server_name, provider_name );
      // the map to store the status of all channels for all boards separately for in and out
      //std::map< std::string, std::map<std::string, std::vector<float>> > chStatusMap;

      if( debug ){
        std::cout << " Requesting = "; 
        for (TString h: desired_histograms) std::cout << h << " ";
        std::cout << std::endl;
        std::cout << pit.entries() << " OH providers found on server '" << server_name << "' in partition '" << partition.name() << "':" << std::endl;
      }
      while ( pit++ ) {

          if( debug ) std::cout << "    " << pit.name( ) << "\t" << ( pit.isActive() ? "[alive]" : "[absent]" );
          // do not consider outdated providers and empty histo names
          if ( !pit.isActive() || desired_histograms.size() == 0 ) {
            if( debug ) std::cout << std::endl;
            continue;
          }
          
          // get ready to iterate over histos
          Iterator hit( partition, server_name, pit.name(), ".*");
          if( debug )
            std::cout << " contains " << hit.entries() << " histogram(s)" << std::endl;
          if( hit.entries() == 0 ) {
            std::cout << "No histos found." << std::endl;
            continue;
          }

          // iterate over histos
          while ( hit() ){
            if( debug ) std::cout << "test: " << pit.name()<<" "<<hit.name() << "\t" << hit.time() << std::endl;

            // check if histogram is desired
            TString h_name(hit.name());
            h_name = h_name(0,h_name.Length()-3);
            for (TString h: desired_histograms){
            	if (h==h_name){
            		// get a histogram
            		OHRootHistogram ohh = OHRootReceiver::getRootHistogram( partition, server_name, pit.name(), hit.name() );
                TH1* this_histogram = (TH1*)ohh.histogram->Clone();
            		saved_histograms.push_back(this_histogram);
            		break;
            	}
            }
         }
      }
    }
    catch( ers::Issue & ex )
    {
        ers::error( ex );
        return {};
    }
    std::cout << "debugthis" << std::endl; 
    return saved_histograms;  
}*/

#endif
