#include "ftkdqm/gnamFTKEventFragment.h"
#include "ftkdqm/gnamFTKExceptions.h"							//  gnam FTK exception 

#include <ers/ers.h>
#include <sstream> 
#include <string>
// emon
#include <emon/EventIterator.h>
#include <eformat/eformat.h>


using namespace daq::ftk;

//=======================================
//
//=======================================
gnamFTKEventFragment::gnamFTKEventFragment()
{
}
//=======================================
//
//=======================================
gnamFTKEventFragment::~gnamFTKEventFragment()
{
}
//=======================================
//
//=======================================
void gnamFTKEventFragment::parseFragment(const std::vector<unsigned int>& data)
{
  parseFragment(&data[0], data.size()); 
}
//=======================================
//
//=======================================
void gnamFTKEventFragment::parseFragment(const uint32_t *data, uint32_t /*size*/)
{
  this->Reset();

  // get event fragment from bitstream
  eformat::read::FullEventFragment fe( data );
  if(!fe.check_noex()){
    ers::error(gnamFTK::GnamFTKDecodeEx(ERS_HERE, "FullEventFragment: Invalid fragment, return."));
    return;
  }

  // header
  this->setRunNumber( fe.run_no() );
  this->setL1ID(      fe.lvl1_id() );
  this->setBCID(      fe.bc_id() );
  this->setLumiBlock( fe.lumi_block() );
  // trailer
  // TODO add if needed


  // read all robs. TODO In case there are many non-FTK robs this might be overkill
  std::vector< eformat::read::ROBFragment > ROBFragments_v;
  fe.robs( ROBFragments_v );
  for(auto& ROBFragment : ROBFragments_v){
    eformat::helper::SourceIdentifier ROBSource_id = eformat::helper::SourceIdentifier( ROBFragment.source_id() );
    if(ROBSource_id.subdetector_id() != eformat::TDAQ_FTK ){
      //expected, too many messages - ers::warning(gnamFTK::GnamFTKDecodeEx(ERS_HERE, "ROBFragment: subdetector not FTK, continue.")); 
      continue;
    }
    const uint32_t *rod_data = ROBFragment.rod_data();
    uint32_t rod_ndata = ROBFragment.rod_ndata();

    if( ( rod_ndata - m_trackBlockOffsetStart - m_trackBlockOffsetEnd ) % m_trackBlockSize != 0 ){
      //ers::warning(gnamFTK::GnamFTKDecodeEx(ERS_HERE, "ROBFragment: data block does not have expected format, continue."));      
      ERS_DEBUG(2,"m_trackBlockSize " << m_trackBlockSize);
      ERS_DEBUG(2,"m_trackBlockOffsetStart" << m_trackBlockOffsetStart);
      ERS_DEBUG(2,"m_trackBlockOffsetEnd" << m_trackBlockOffsetEnd);
      ERS_DEBUG(2,"Number of tracks in ROBFragment: " << (1.*rod_ndata-m_trackBlockOffsetStart-m_trackBlockOffsetEnd) / m_trackBlockSize);
      continue;
    }
    int nTrks = ( rod_ndata - m_trackBlockOffsetStart - m_trackBlockOffsetEnd ) / m_trackBlockSize;
    //ERS_LOG("numOfTracks"<<nTrks);
    rod_data += m_trackBlockOffsetStart;            // moving data pointer to first track block
    m_tracks.reserve(m_tracks.size() + nTrks);
    for(int i_trk=0; i_trk<nTrks; i_trk++){
      m_tracks.push_back(unpackTrack(rod_data));
      rod_data += m_trackBlockSize;                 // moving data pointer to next track block
    }
  }
}
//=======================================
//
//=======================================
gnamFTKTrack gnamFTKEventFragment::unpackTrack(const uint32_t *dataBlock)
{
  // FTK Data Format v2.4
//uint16_t markerWord = (0x9bda & 0xffff);
//if (markerWord == (dataBlock[0] >> 16) ){
  uint16_t chiSq_bits	= (dataBlock[3] >> 16);		  // high bits 16-32
  uint16_t d0_bits 	= (dataBlock[3] & 0xffff);	  // low bits 0-15
  uint16_t z0_bits      = (dataBlock[4] >> 16);
  uint16_t cotTh_bits	= (dataBlock[4] & 0xffff);
  uint16_t phi0_bits    = (dataBlock[5] >> 16);
  uint16_t curv_bits 	= (dataBlock[5] & 0xffff);
  uint16_t tower =( (dataBlock[1] >> 16) & 0x7f) ;
  uint8_t nPixHits(0);
  for(int i_pix=0; i_pix<4; i_pix++){
    // hit if data word not empty
    if( dataBlock[6+2*i_pix] != 0x0 ) nPixHits++;
      }
  uint8_t nSctHits(0);
  for(int i_sct=0; i_sct<8; i_sct++){
    // hit if data word not empty
    if( dataBlock[14+i_sct] != 0x0 ) nSctHits++;
  }
  uint16_t layermap = (dataBlock[1] & 0xFFF);         // low bits 0-11
  return gnamFTKTrack(chiSq_bits, d0_bits, z0_bits, cotTh_bits, phi0_bits, curv_bits, tower, nPixHits, nSctHits, layermap);
 }
//}
//=======================================
//
//=======================================
std::vector<unsigned int> gnamFTKEventFragment::bitstream() const
{
  //TODO
  ERS_LOG("gnamFTKEventFragment::bitstream() not implemented yet... sorry");
  return std::vector<unsigned int>();
}
//=======================================
//
//=======================================
const std::vector<daq::ftk::gnamFTKTrack> *gnamFTKEventFragment::getTracks( void ) const
{
  return &m_tracks;
}
//=======================================
//
//=======================================
std::string gnamFTKEventFragment::Print( void ) const
{
  std::stringstream out_ss;
  out_ss << "gnamFTKEventFragment::Print\n"
    << "--> m_l1id: "   << std::hex << this->getL1ID()
    << " m_bcid: "   << std::hex << this->getBCID()
    << " m_runNr: "  << std::dec << this->getRunNumber() << "\n"
    << "--> number of tracks " << std::dec << this->getNumTracks() << "\n";
  for(auto& track : m_tracks)
    out_ss << track.Print( 0 );    
  return out_ss.str();
}
//=======================================
//
//=======================================
void gnamFTKEventFragment::Reset( void )
{
  setL1ID(-1);
  setBCID(-1);
  setLumiBlock(-1);
  setRunNumber(-1);
  setTrackBlockOffsetStart(0);
  setTrackBlockOffsetEnd(6);
  m_tracks.clear();
}
