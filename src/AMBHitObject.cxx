#include "ftkdqm/AMBHitObject.h"

#include <iostream>
#include <iomanip>

namespace daq {
  namespace ftk {
    
    AMBHitObject::AMBHitObject()
    { }
    
    AMBHitObject::AMBHitObject(unsigned data,
			       unsigned ssID){
      this->data = data;
      this->ssID = ssID;
    }

    AMBHitObject::AMBHitObject(const AMBHitObject &obj){ 
      this->data = obj.data;
      this->ssID = obj.ssID;
    }

    AMBHitObject::~AMBHitObject()
    { }

    unsigned AMBHitObject::getData() const
    { return data; }
    
    unsigned AMBHitObject::getSSID() const
    { return ssID; }

    void AMBHitObject::print()
    { 
      std::cout << std::setw(8) 
		<< std::setfill('0') 
		<< std::right 
		<< std::hex 
		<< data
		<<" (SSID:"<<ssID
		<<")"<<std::endl;
    }
  }

}

