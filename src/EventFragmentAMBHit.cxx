#include "ftkdqm/EventFragmentAMBHit.h"

#include <iostream>
#include <iomanip>

namespace daq {
  namespace ftk {
    
    EventFragmentAMBHit::EventFragmentAMBHit()
      : EventFragment()
    { }
    
    EventFragmentAMBHit::~EventFragmentAMBHit()
    { }
    
    std::vector<unsigned> EventFragmentAMBHit::getData() const
    { return _data; }

    AMBHitObjectEvent EventFragmentAMBHit::getEventHits() const
    { return eventHits; }
    
    void EventFragmentAMBHit::parseFragment(const std::vector<unsigned> &data)
    {
      // Read meta-data
      //setL1ID(data[0] & 0xffff);
      int ee_ind = data.size()-1;      
      setL1ID(data[ee_ind] & 0xffff);
      
      // Parse the things
      _data = data;

      // Kill the first two words (b0f and L1ID)
      //_data.erase(_data.begin());
      _data.erase(_data.begin()+ee_ind);

      unsigned nHit = _data.size();

      eventHits.setL1ID(data[ee_ind] & 0xffff);

      int nActualHit = 0;

      for(int i=0; i<nHit; i++){
	
	if((_data.at(i)>>16)&0xffff != 0x0000){
	  AMBHitObject aho(_data.at(i),//data
			   (_data.at(i)>>16)&0xffff);//ssID
	  eventHits.pushHit(aho);
	  ++nActualHit;
	}

	if(_data.at(i)&0xffff != 0x0000){
	  AMBHitObject aho(_data.at(i),//data
			   _data.at(i)&0xffff);//ssID
	  eventHits.pushHit(aho);
	  ++nActualHit;
	}
      }

      eventHits.setNHit(nActualHit);

    }
        
    std::vector<unsigned> EventFragmentAMBHit::bitstream() const
    {
      //Note: the following is a dummy 10/06/2015
      std::vector<unsigned> result;
      result.push_back(0xb0b00d01);
      result.push_back(getL1ID());
      result.insert(result.end(), _data.begin(), _data.end());
      return result;
    }
    
    void EventFragmentAMBHit::Reset( void )
    {
      setL1ID(-1);
      setBCID(-1);
      setRunNumber(-1);

      _data.clear();
    }

    std::vector<EventFragment*> AMBHit_splitFragments(const std::vector<unsigned>& data)
    {

      std::vector<daq::ftk::EventFragment*> events;
      
      std::vector<unsigned> eventData;
      
      daq::ftk::EventFragment *event=0;
      for(unsigned i=0;i<data.size();i++)
	{

	  // Add word to data list
	  eventData.push_back(data[i]);
	  
	  // Close an event
	  if(((data[i]>>20)&0xfff)==0xf70)
	    {
	      if(event!=0) 
		{
		  event->parseFragment(eventData);
		  events.push_back(event);
		}
	      
	      // Create new event
	      event=new daq::ftk::EventFragmentAMBHit();
	      eventData.clear();
	    }
	  
	}
      
      delete event;
      //eventData.pop_back(); // Kill last event since it might be incomplete

      return events;
      
    }
    
  }
}

