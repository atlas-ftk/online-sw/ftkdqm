/* gnamAMBDecode.cxx
 * author: Takashi.Kubota@cern.ch
 * started: 01/11/2016
 *
 * implementation of gnam decoder to read events from emon 
 * and store them in the gnamFTKEventFragment class
 */

// GNAM: 
#include <gnam/gnamutils/GnamUtils.h>				//  global include file for libraries

// TDAQ: 
#include <config/Configuration.h>				//  OKS DB entry point
#include <dal/util.h>					        //  for DB object casting

// my includes: 
#include "ftkdqm/EventFragmentAMBRoad.h"	    //  definition of the class correspoding to my event
#include "ftkdqm/EventFragmentAMBHit.h"	    //  definition of the class correspoding to my event
#include "gnamAMBLibdal/gnamAMBDecodeLib.h" //  definition of the DB object correspoding to my library

// std
#include <stdlib.h>
#include <errno.h>

// ers
#include <ers/ers.h>

// ftk
#include "ftkcommon/exceptions.h"
#include "ftkcommon/core.h"
#include "ftkcommon/EventFragmentCollection.h"
#include "ftkcommon/SourceIDSpyBuffer.h"
#include "ftkdqm/gnamFTKExceptions.h"

// emon
#include <emon/EventIterator.h>
#include <eformat/eformat.h>

/* GNAM
 *
 * Use *one* instance for each one of your data classes; "SetBranch" will
 * record the address of this instance and it cannot change until next run.
 */
static std::vector<daq::ftk::EventFragmentCollection*> * myRoadFragment = NULL;
static std::vector<daq::ftk::EventFragmentCollection*> * myHitFragment = NULL;

/* 
 * GNAM
 * "initDB" function: called once just after the library has been loaded
 * in case of configuration from OKS DB.
 * You should collect your configuration parameters from the DB.
 */
  extern "C" void
initDB (Configuration * confDB, const gnamdal::GnamLibrary * library)
{
  ERS_LOG("gnamAMBDecode::initDB()");
  /*
   * GNAM
   * In order to read your configuration parameters, you 
   * need to downcast the abstract object "library" to 
   * the type of your library 
   */
  const gnamAMBLibdal::gnamAMBDecodeLib * dummyLib =
    confDB->cast<gnamAMBLibdal::gnamAMBDecodeLib,
    gnamdal::GnamLibrary>(library);

  myRoadFragment = new std::vector<daq::ftk::EventFragmentCollection*>();
  myHitFragment = new std::vector<daq::ftk::EventFragmentCollection*>();

  /*
   * GNAM
   * Use GNAM_BRANCH_REGISTER with 2 parameters:
   *    The 1st parameter is the name of the branch.
   *    The 2nd parameter is the pointer to the object to be stored. 
   */
  try{
    GNAM_BRANCH_REGISTER ("AMBRoadEventFragmentCollectionBranch", myRoadFragment);
  }
  catch (daq::gnamlib::AlreadyExistingBranch &exc) {
    throw daq::gnamlib::CannotRegisterBranch (ERS_HERE, "AMBRoadEventFragmentCollectionBranch");
  }

  try{
    GNAM_BRANCH_REGISTER ("AMBHitEventFragmentCollectionBranch", myHitFragment);
  }
  catch (daq::gnamlib::AlreadyExistingBranch &exc) {
    throw daq::gnamlib::CannotRegisterBranch (ERS_HERE, "AMBHitEventFragmentCollectionBranch");
  }

}

/* 
 * GNAM
 * "end" function: called once just before the library will be unloaded.
 * You should put here your final clean-up tasks.
 */
  extern "C" void
end (void)
{
  ERS_LOG("gnamAMBDecode::end()");
  delete myRoadFragment;
  delete myHitFragment;
}

/* 
 * GNAM
 * "decode" function: here you should analyze the data
 * and fill the shared objects 
 */
  extern "C" void
decode (const std::vector<uint32_t const *> * /*rods*/ ,
    const std::vector<unsigned long int> * /*sizes*/ ,
    const uint32_t *event, unsigned long int event_size)
{
  //ERS_LOG("gnamAMBDecode::decode()");

  myRoadFragment->clear();
  myHitFragment->clear();

  eformat::read::FullEventFragment fe( event );
  if(!fe.check_noex()){
    ers::error(daq::gnamFTK::GnamFTKDecodeEx(ERS_HERE, "FullEventFragment: Invalid fragment, return."));
    return;
  }

  std::vector<std::vector<daq::ftk::EventFragment*>> roadfragmentss; roadfragmentss.clear();
  std::vector<std::vector<daq::ftk::EventFragment*>> hitfragmentss; hitfragmentss.clear();
  
  std::vector< eformat::read::ROBFragment > ROBFragments_v;
  fe.robs( ROBFragments_v );
  for(auto& ROBFragment : ROBFragments_v){
    
    eformat::helper::SourceIdentifier ROBSource_id = eformat::helper::SourceIdentifier( ROBFragment.source_id() );
    if(ROBSource_id.subdetector_id() != eformat::TDAQ_FTK ){
      ers::warning(daq::gnamFTK::GnamFTKDecodeEx(ERS_HERE, "ROBFragment: subdetector not FTK, continue.")); 
      continue;
    }
    
    daq::ftk::SourceIDSpyBuffer SBSource_id = daq::ftk::decode_SourceIDSpyBuffer( ROBFragment.source_id() );
    if(SBSource_id.boardType != daq::ftk::BoardType::AMB ){
      ers::warning(daq::gnamFTK::GnamFTKDecodeEx(ERS_HERE, "BoardType is not AMB, continue.")); 
      continue;
    }

    // For Road FPGA
    if(SBSource_id.position == daq::ftk::Position::OUT ){
      const uint32_t *p_data = ROBFragment.rod_data();
      uint32_t rod_ndata = ROBFragment.rod_ndata();
      
      std::vector<unsigned int> data(p_data, p_data+rod_ndata);

      std::vector<daq::ftk::EventFragment*> events = daq::ftk::AMBRoad_splitFragments(data);
      if(events.size() != 0) roadfragmentss.push_back(events);
    
    // For Hit FPGA
    } else if(SBSource_id.position == daq::ftk::Position::IN ) {
      const uint32_t *p_data = ROBFragment.rod_data();
      uint32_t rod_ndata = ROBFragment.rod_ndata();
      
      std::vector<unsigned int> data(p_data, p_data+rod_ndata);

      std::vector<daq::ftk::EventFragment*> events = daq::ftk::AMBHit_splitFragments(data);
      if(events.size() != 0) hitfragmentss.push_back(events);
      
    }

  }

  std::vector<daq::ftk::EventFragmentCollection*> efc_vec_road;
  efc_vec_road = daq::ftk::build_eventFragmentCollection(roadfragmentss,true);
  for(uint32_t iefc=0; iefc<efc_vec_road.size(); iefc++) myRoadFragment->push_back(efc_vec_road.at(iefc));
  
  std::vector<daq::ftk::EventFragmentCollection*> efc_vec_hit;
  efc_vec_hit = daq::ftk::build_eventFragmentCollection(hitfragmentss,true);
  for(uint32_t iefc=0; iefc<efc_vec_hit.size(); iefc++) myHitFragment->push_back(efc_vec_hit.at(iefc));
  
}


