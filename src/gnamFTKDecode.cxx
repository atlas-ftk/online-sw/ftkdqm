/* gnamFTKDecode.cxx
 * author: Arthur.Bolz@cern.ch
 * started: 19/10/2015
 *
 * implementation of gnam decoder to read events from emon 
 * and store them in the gnamFTKEventFragment class
 */

// GNAM: 
#include <gnam/gnamutils/GnamUtils.h>				//  global include file for libraries

// TDAQ: 
#include <config/Configuration.h>						//  OKS DB entry point
#include <dal/util.h>												//  for DB object casting

// my includes: 
#include "ftkdqm/gnamFTKEventFragment.h"			//  definition of the class correspoding to my event
#include "ftkdqm/gnamFTKTrack.h"							//  FTK track class
#include "ftkdqm/gnamFTKExceptions.h"							//  gnam FTK exception 
#include "gnamFTKLibdal/gnamFTKDecodeLib.h" //  definition of the DB object correspoding to my library

// std
#include <stdlib.h>
#include <errno.h>


// ers
#include <ers/ers.h>

// ftk
#include "ftkcommon/exceptions.h"
#include "ftkcommon/core.h"

/* GNAM
 *
 * Use *one* instance for each one of your data classes; "SetBranch" will
 * record the address of this instance and it cannot change until next run.
 */
static daq::ftk::gnamFTKEventFragment * myFragment = NULL;

/* 
 * GNAM
 * "initDB" function: called once just after the library has been loaded
 * in case of configuration from OKS DB.
 * You should collect your configuration parameters from the DB.
 */
  extern "C" void
initDB (Configuration * confDB, const gnamdal::GnamLibrary * library)
{
  ERS_LOG("gnamFTKDecode::initDB()");
  /*
   * GNAM
   * In order to read your configuration parameters, you 
   * need to downcast the abstract object "library" to 
   * the type of your library 
   */
  const gnamFTKLibdal::gnamFTKDecodeLib * dummyLib =
    confDB->cast<gnamFTKLibdal::gnamFTKDecodeLib,
    gnamdal::GnamLibrary>(library);

  myFragment = new daq::ftk::gnamFTKEventFragment();
  myFragment->setTrackBlockOffsetStart(dummyLib->get_trackBlockOffsetStart());
  myFragment->setTrackBlockOffsetEnd(dummyLib->get_trackBlockOffsetEnd());

  /*
   * GNAM
   * Use GNAM_BRANCH_REGISTER with 2 parameters:
   *    The 1st parameter is the name of the branch.
   *    The 2nd parameter is the pointer to the object to be stored. 
   */
  try{
    GNAM_BRANCH_REGISTER ("gnamFTKEventFragmentBranch", myFragment);
  }
  catch (daq::gnamlib::AlreadyExistingBranch &exc) {
    throw daq::gnamlib::CannotRegisterBranch (ERS_HERE, "gnamFTKEventFragmentBranch");
  }

  ERS_LOG("myFragment = " << myFragment); 
}

/* 
 * GNAM
 * "end" function: called once just before the library will be unloaded.
 * You should put here your final clean-up tasks.
 */
  extern "C" void
end (void)
{
  ERS_LOG("gnamFTKDecode::end()");
  delete myFragment;
}

/* 
 * GNAM
 * "decode" function: here you should analyze the data
 * and fill the shared objects 
 */
  extern "C" void
decode (const std::vector<uint32_t const *> * /*rods*/ ,
    const std::vector<unsigned long int> * /*sizes*/ ,
    const uint32_t *event, unsigned long int event_size)
{
  //ERS_LOG("gnamFTKDecode::decode()");
  myFragment->parseFragment(event, event_size);
  //ERS_LOG(myFragment->Print());
}


