#include "ftkdqm/EventFragmentAMBRoad.h"

#include <iostream>
#include <iomanip>

// emon
#include <emon/EventIterator.h>
#include <eformat/eformat.h>

namespace daq {
  namespace ftk {
    
    EventFragmentAMBRoad::EventFragmentAMBRoad()
      : EventFragment()
    { }
    
    EventFragmentAMBRoad::~EventFragmentAMBRoad()
    { }
    
    std::vector<unsigned> EventFragmentAMBRoad::getData() const
    { return _data; }

    AMBRoadObjectEvent EventFragmentAMBRoad::getEventRoads() const
    { return eventRoads; }
    
    void EventFragmentAMBRoad::parseFragment(const std::vector<unsigned int>& data)
    {
      // Read meta-data
      //setL1ID(data[0] & 0xffff);
      
      int ee_ind = data.size()-1;      
      setL1ID(data[ee_ind] & 0xffff);
      
      // Parse the things
      _data = data;
      
      // Kill the first two words (b0f and L1ID)
      //_data.erase(_data.begin());
      _data.erase(_data.begin()+ee_ind);
      
      unsigned nRoad = _data.size();
      
      eventRoads.setL1ID(data[ee_ind] & 0xffff);
      eventRoads.setNRoad(nRoad);
      
      for(int i=0; i<nRoad; i++){
	
	int nLayerHit=0;
	std::vector<unsigned> mLayers;
	for(int ibit=0; ibit<8; ibit++) {
	  if( ((((_data.at(i)>>24)&0xff)>>ibit)&1)==1 )  ++nLayerHit;
	  else mLayers.push_back(ibit);
	}
	
	AMBRoadObject aro(_data.at(i),//data
			  (_data.at(i)>>24)&0xff,//bitMap
			  nLayerHit,//numberOfHits
			  mLayers,//missingLayers
			  (_data.at(i)>>17)&0x3,//chip
			  (_data.at(i)>>19)&0x3,//link
			  (_data.at(i)>>21)&0x3,//lamb
			  _data.at(i)&0x1ffff);//patternAddress
	
	eventRoads.pushRoad(aro);
	
      }

    }
        
    std::vector<unsigned> EventFragmentAMBRoad::bitstream() const
    {
      //Note: the following is a dummy 10/06/2015
      std::vector<unsigned> result;
      result.push_back(0xb0b00d01);
      result.push_back(getL1ID());
      result.insert(result.end(), _data.begin(), _data.end());
      return result;
    }

    void EventFragmentAMBRoad::Reset( void )
    {
      setL1ID(-1);
      setBCID(-1);
      setRunNumber(-1);

      _data.clear();
    }
    
    std::vector<EventFragment*> AMBRoad_splitFragments(const std::vector<unsigned>& data)
    {

      std::vector<daq::ftk::EventFragment*> events;
      
      std::vector<unsigned> eventData;
      
      daq::ftk::EventFragment *event=0;
      for(unsigned i=0;i<data.size();i++)
	{

	  // Add word to data list
	  eventData.push_back(data[i]);
	  // Close an event
	  if(((data[i]>>20)&0xfff)==0xf78)
	    {
	      if(event!=0) 
		{
		  event->parseFragment(eventData);
		  events.push_back(event);
		}
	      
	      // Create new event
	      event=new daq::ftk::EventFragmentAMBRoad();
	      eventData.clear();
	    }
	  
	}

      delete event;
      //eventData.pop_back(); // Kill last event since it might be incomplete

      return events;
      
    }
    
  }
}
