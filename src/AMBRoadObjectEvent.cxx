#include "ftkdqm/AMBRoadObjectEvent.h"

#include <iostream>
#include <iomanip>

namespace daq {
  namespace ftk {
    
    AMBRoadObjectEvent::AMBRoadObjectEvent()
    { }

    AMBRoadObjectEvent::AMBRoadObjectEvent(unsigned l1ID,
					   unsigned nRoad,
					   std::vector<AMBRoadObject> roads)
    { this->l1ID=l1ID;
      this->nRoad=nRoad;
      this->roads=roads;}
    
    AMBRoadObjectEvent::~AMBRoadObjectEvent()
    { }

    unsigned AMBRoadObjectEvent::getNRoad() const
    { return nRoad; }

    unsigned AMBRoadObjectEvent::getL1ID() const
    { return l1ID; }
    
    std::vector<AMBRoadObject> AMBRoadObjectEvent::getRoads() const
    { return roads; }

    void AMBRoadObjectEvent::setNRoad(unsigned ui)
    { nRoad = ui; }

    void AMBRoadObjectEvent::setL1ID(unsigned ui)
    { l1ID = ui; }

    void AMBRoadObjectEvent::setRoads(std::vector<AMBRoadObject> v)
    { roads = v; }

    void AMBRoadObjectEvent::pushRoad(AMBRoadObject aro)
    { roads.push_back(aro); }

  }
}

