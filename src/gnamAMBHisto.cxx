/* gnamAMBHisto.cxx
 * author: Takashi.Kubota@cern.ch
 * started: 01/11/2016
 *
 * implementation of gnam histo prepare histograms and fill 
 * them from information stored in gnamFTKEventFragment class
 */

// GNAM: 
#include <gnam/gnamutils/GnamUtils.h> // global include file for libraries

// TDAQ: 
#include <config/Configuration.h> // OKS DB entry point
#include <dal/util.h>             // for DB object casting

// my includes
#include "ftkdqm/EventFragmentAMBRoad.h"	    //  definition of the class correspoding to my event
#include "ftkdqm/EventFragmentAMBHit.h"	    //  definition of the class correspoding to my event
#include "gnamAMBLibdal/gnamAMBHistoLib.h"  //  definition of the DB object correspoding to my library

// ftk
#include "ftkcommon/EventFragmentCollection.h"

// std
#include <stdlib.h>
#include <errno.h>
#include <string>
#include <sstream> 

// root
#include <TH1.h>
#include <TH2.h>
#include <TProfile.h>
#include <TString.h> // Form
#include <TFile.h> // Form

/* 
 * GNAM
 * Use *one* instance for each one of your data histograms; their addresses
 * will be added to a list after calling "bookHisto" and the list cannot
 * change until next run.
 */
static GnamHisto *h1_lastl1id,
                 *h1_nroads,
                 *h1_geoaddress,
                 *h1_nhits;

/*
 * GNAM
 * configuration parameters
 */
static int nBinsL1id,
           nBinsNroads,
           nBinsGeoaddress,
           nBinsNhits;

static double l1idMax,
              nroadsMax,
              geoaddressMax,
              nhitsMax;

// flag for histograms with axis limits to be determined during runtime
static bool firstCall = true; 

/*
 * GNAM
 * Use this syntax for the histogram list. This will assure 
 * a clean library unloading. Look at the sintax also in
 * the "internal_init" and "end" functions  
 */
static std::vector<GnamHisto*> *histolist = NULL;
/*
 * GNAM
 * "MyObject" will point to the contents of the branch "MyClassBranch",
 * which was filled by the decoding routine.
 * For efficiency reasons, it is recommended to declare it static.
 * Declare here, so it can be set to NULL in Cleanup.
 *
 */
static const std::vector<daq::ftk::EventFragmentCollection*> * myRoadFragment = NULL;
static const std::vector<daq::ftk::EventFragmentCollection*> * myHitFragment = NULL;

/*
 * Cleanup
 * Delete all pointers. Don't just loop over histolist and delete as it will cause
 * problems with TowerHistos destructor"
 */

void Cleanup(void)
{
  ERS_LOG("gnamAMBHisto::Cleanup()");
  if (histolist != NULL) { 
    delete histolist; histolist = NULL;
    
    delete h1_lastl1id; h1_lastl1id = NULL;
    delete h1_nroads; h1_nroads = NULL;
    delete h1_geoaddress; h1_geoaddress = NULL;
    delete h1_nhits; h1_nhits = NULL;

  }
  // set NULL or GNAM_BRANCH_FILL won't set to new EventFragment instance
  // after doing Unconfig -> Config -> Start in RC
  myRoadFragment = NULL;
  myHitFragment = NULL;

}

/* 
 * GNAM
 * "initDB" function: called once just after the library has been loaded
 * in case of configuration from OKS DB.
 * You should collect your configuration parameters from the DB.
 */
  extern "C" void
initDB (Configuration * confDB, const gnamdal::GnamLibrary * library)
{
  ERS_LOG("gnamAMBHisto::initDB()");
  /* 
   * In order to read your configuration parameters, you 
   * need to downcast the abstract opject "library" to 
   * the type of your library 
   */
  const gnamAMBLibdal::gnamAMBHistoLib * histoLib =
    confDB->cast<gnamAMBLibdal::gnamAMBHistoLib,
    gnamdal::GnamLibrary>(library);

  nBinsL1id       = histoLib->get_nBinsL1id();
  nBinsNroads     = histoLib->get_nBinsNroads();
  nBinsGeoaddress = histoLib->get_nBinsGeoaddress();
  nBinsNhits      = histoLib->get_nBinsNhits();
  l1idMax         = histoLib->get_l1idMax();
  nroadsMax       = histoLib->get_nroadsMax();
  geoaddressMax   = histoLib->get_geoaddressMax();
  nhitsMax        = histoLib->get_nhitsMax();

  std::stringstream out_ss;
  out_ss << "Histogram binning parameters:"
	 << "\nnBinsL1id = " << nBinsL1id
	 << "\nnBinsNRoads = " << nBinsNroads
	 << "\nnBinsGeoaddress = " << nBinsGeoaddress
	 << "\nnBinsNHits = " << nBinsNhits
	 << "\nl1idMax = " << l1idMax
	 << "\nnroadsMax = " << nroadsMax
	 << "\ngeoaddressMa = " << geoaddressMax
	 << "\nnhitsMax = " << nhitsMax << ", ";
  
  ERS_LOG(out_ss.str());
}

/* 
 * GNAM
 * "end" function: called once just before the library will be unloaded.
 * You should put here your final clean-up tasks.
 */
  extern "C" void
end (void)
{
  ERS_LOG("gnamAMBHisto::end()");

  Cleanup();
}

/* 
 * GNAM
 * "fillHisto" function: look for your branch (or branches)
 * in the data storage and fill your histograms.
 */
  extern "C" void
fillHisto (void)
{
  
  //ERS_LOG("gnamAMBHisto::fillHisto()");
  
  if(firstCall)
    ERS_LOG("gnamAMBHisto::fillHisto() first call");
  try {
    GNAM_BRANCH_FILL ("AMBRoadEventFragmentCollectionBranch", std::vector<daq::ftk::EventFragmentCollection*>, myRoadFragment);
  }
  catch (daq::gnamlib::NotExistingBranch &exc) {
    throw daq::gnamlib::CannotFindBranch (ERS_HERE, "AMBRoadEventFragmentCollectionBranch");
  }

  try {
    GNAM_BRANCH_FILL ("AMBHitEventFragmentCollectionBranch", std::vector<daq::ftk::EventFragmentCollection*>, myHitFragment);
  }
  catch (daq::gnamlib::NotExistingBranch &exc) {
    throw daq::gnamlib::CannotFindBranch (ERS_HERE, "AMBHitEventFragmentCollectionBranch");
  }

  // Filling Road spybuffers
  if(myRoadFragment->size() > 0) {
    int tmp_lvl1id=0;
    for (uint32_t iev=0; iev<myRoadFragment->size(); iev++) {
      for(uint32_t isb=0; isb<(myRoadFragment->at(iev))->getNEventFragments(); isb++) {  
	daq::ftk::EventFragmentAMBRoad* efar = (static_cast<daq::ftk::EventFragmentAMBRoad*>((myRoadFragment->at(iev))->getEventFragment(isb)));
	daq::ftk::AMBRoadObjectEvent aroe = efar->getEventRoads();
	if(aroe.getL1ID() > tmp_lvl1id) tmp_lvl1id=aroe.getL1ID();
	h1_nroads->Fill(aroe.getNRoad());
	for(uint32_t ird=0; ird<aroe.getNRoad(); ird++) {
	  daq::ftk::AMBRoadObject aro = (aroe.getRoads()).at(ird);
	  h1_geoaddress->Fill(aro.getGeoAddress());	
	}
      }
    }
    h1_lastl1id->Fill(tmp_lvl1id);
  }

  // Filling Hit spybuffers
  for (uint32_t iev=0; iev<myHitFragment->size(); iev++) {
    for(uint32_t isb=0; isb<(myHitFragment->at(iev))->getNEventFragments(); isb++) {  
	daq::ftk::EventFragmentAMBHit* efah = (static_cast<daq::ftk::EventFragmentAMBHit*>((myHitFragment->at(iev))->getEventFragment(isb)));
	daq::ftk::AMBHitObjectEvent ahoe = efah->getEventHits();
	h1_nhits->Fill(ahoe.getNHit());	
      }
  }
  
  for (uint32_t i=0; i<myRoadFragment->size(); i++) delete myRoadFragment->at(i);
  for (uint32_t i=0; i<myHitFragment->size(); i++) delete myHitFragment->at(i);

  firstCall = false;
  
}

/* 
 * GNAM
 * "startOfRun" function: called once every start of run;
 * here you can prepare your library for the histogram filling.
 * "run" is the run number. For examples see gnamFTK and gnam::GnamHisto
 */
  extern "C" const std::vector<GnamHisto*> *
startOfRun (int /*run*/,std::string /*type*/)
{
  ERS_LOG("gnamAMBHisto::startOfRun()");
  ERS_ASSERT (histolist == NULL);
  // Use this syntax
  histolist = new std::vector<GnamHisto *>;
  firstCall = true;

  //needs to have this path structure /SHIFT/, /EXPERT/, /RUNSTAT/, or /DEBUG/ + SUBPATH/
  const std::string expertPath = "/EXPERT/FTK/"; 

  h1_lastl1id = new GnamHisto(Form("%sh1_lastl1id", expertPath.c_str()),
      Form(";Level-1 ID;#events"),
      nBinsL1id, -0.5, l1idMax, 
      GnamHisto::INTBIN);
  histolist->push_back(h1_lastl1id);

  h1_nroads = new GnamHisto(Form("%sh1_nroads", expertPath.c_str()),
      Form(";# of roads;#events"),
      nBinsNroads, -0.5, nroadsMax, 
      GnamHisto::INTBIN);
  histolist->push_back(h1_nroads);

  h1_geoaddress = new GnamHisto(Form("%sh1_geoaddress", expertPath.c_str()),
      Form(";Geographical address;#events"),
      nBinsGeoaddress, -0.5, geoaddressMax, 
      GnamHisto::INTBIN);
  histolist->push_back(h1_geoaddress);
  
  h1_nhits = new GnamHisto(Form("%sh1_nhits", expertPath.c_str()),
      Form(";# of hits;#events"),
      nBinsNhits, -0.5, nhitsMax, 
      GnamHisto::INTBIN);
  histolist->push_back(h1_nhits);

  ERS_LOG(std::to_string(histolist->size()) + " regular histos booked");
  return histolist;
}

/* 
 * GNAM
 * "prePublish" function: here you can fill special histograms just
 * before the publication.
 */
  extern "C" void
prePublish (bool endOfRun)
{
  ERS_LOG("gnamAMBHisto::prePublish()");

  //firstCall = true;

  if (endOfRun) {
    ERS_LOG("end of run");
  }
}

/*
 * GNAM
 * "endOfRun" function: called once every end of run,
 * here you make some clean-up
 */
  extern "C" void
endOfRun (void)
{
  ERS_LOG("gnamAMBHisto::endOfRun()");

  Cleanup();
}

/* 
 * GNAM
 * "DFStopped" function: this function is called after endOfRun, when 
 * the DataFlow is completely stopped. You shoudl not update your
 * histograms here since they aer not going to be published by Gnam. 
 * The function is here mainly for MonaIsa support (IS gathering after the
 * end of run).
 */
  extern "C" void
DFStopped (void)
{
  ERS_LOG("gnamAMBHisto::DFStopped()");
}


/*
 * GNAM
 * "customCommand" function: here you will receive custom command from OH.
 * You will have also the parameters of your command 
 */
  extern "C" void
customCommand (const GnamHisto*  /*histo*/ , int /*argc*/ , const char * const* /*argv*/)
{
  ERS_LOG("gnamAMBHisto::customCommand()");

}
