#include "ftkdqm/gnamFTKTrack.h"


#include <ers/ers.h>
#include <sstream> 
#include <iomanip>

using namespace daq::ftk;

//=======================================
//
//=======================================
gnamFTKTrack::gnamFTKTrack()
{
}
//=======================================
//
//=======================================
gnamFTKTrack::gnamFTKTrack(
    uint16_t chiSq_bits, uint16_t d0_bits, uint16_t z0_bits, uint16_t cotTh_bits, uint16_t phi0_bits, uint16_t curv_bits, uint16_t tower,
    uint8_t nPixHits, uint8_t nSctHits, uint16_t layermap)
{
  SetParameters(chiSq_bits, d0_bits, z0_bits, cotTh_bits, phi0_bits, curv_bits, tower, nPixHits, nSctHits, layermap);
}
//=======================================
//=======================================
std::string gnamFTKTrack::Print( int mode ) const
{
  std::stringstream out_ss;
  if(mode == 0)
    out_ss << std::hex 
      << "d0    " << std::setw(10) << m_d0_bits    << "\n"
      << "z0    " << std::setw(10) << m_z0_bits    << "\n"
      << "cotTh " << std::setw(10) << m_cotTh_bits << "\n"
      << "phi0  " << std::setw(10) << m_phi0_bits  << "\n"
      << "chiSq " << std::setw(10) << m_chiSq_bits << "\n"
      << "curv  " << std::setw(10) << m_curv_bits  << "\n";

      if(mode == 1)
        out_ss << std::dec
      << "d0    " << std::setw(10) << m_d0    << "\n"
      << "z0    " << std::setw(10) << m_z0    << "\n"
      << "cotTh " << std::setw(10) << m_cotTh << "\n"
      << "phi0  " << std::setw(10) << m_phi0  << "\n"
      << "chiSq " << std::setw(10) << m_chiSq << "\n"
      << "curv  " << std::setw(10) << m_curv  << "\n"
      << "tower " << std::setw(10) << m_tower << "\n"
      << "nPixHits " << std::setw(10) << m_numb_pix_hits  << "\n"
      << "nSctHits " << std::setw(10) << m_numb_sct_hits  << "\n";
  return out_ss.str();
}
//=======================================
//
//=======================================
void gnamFTKTrack::SetParameters(
    uint16_t chiSq_bits, uint16_t d0_bits, uint16_t z0_bits, uint16_t cotTh_bits, uint16_t phi0_bits, uint16_t curv_bits, uint16_t tower,
    uint8_t nPixHits, uint8_t nSctHits, uint16_t layermap)
{
  m_d0_bits	= d0_bits; 
  m_z0_bits 	= z0_bits;
  m_cotTh_bits 	= cotTh_bits;
  m_phi0_bits 	= phi0_bits;
  m_chiSq_bits 	= chiSq_bits;
  m_curv_bits 	= curv_bits;
 // m_tower_bits  = tower;
  m_d0		= GetD0(); 
  m_z0 		= GetZ0();
  m_cotTh	= GetCotTh();
//ERS_LOG("cot="<<m_cotTh);
  m_phi0 	= GetPhi0();
  m_chiSq 	= GetChiSq();
  m_curv 	= GetCurv();
//ERS_LOG("curv="<<m_curv);
  m_tower       = tower;
  m_theta = GetTheta();
  m_eta = GetEta();
  m_pt      = GetPt(); 
  m_numb_pix_hits = nPixHits;
  m_numb_sct_hits = nSctHits;
  m_layermap      = layermap;
}
//=======================================

//=======================================
bool gnamFTKTrack::GetHitInLayer(int layer) const 
{ 
  bool retval = ( 1 == ( (m_layermap >> layer) & 1) );
  return retval;
}
