#include "ftkdqm/AMBHitObjectEvent.h"

#include <iostream>
#include <iomanip>

namespace daq {
  namespace ftk {
    
    AMBHitObjectEvent::AMBHitObjectEvent()
    { }
    
    AMBHitObjectEvent::AMBHitObjectEvent(unsigned l1ID,
					 unsigned nHit,
					 std::vector<AMBHitObject> hits)
    { this->l1ID=l1ID;
      this->nHit=nHit;
      this->hits=hits;}
    
    AMBHitObjectEvent::~AMBHitObjectEvent()
    { }
    
    unsigned AMBHitObjectEvent::getNHit() const
    { return nHit; }
    
    unsigned AMBHitObjectEvent::getL1ID() const
    { return l1ID; }
    
    std::vector<AMBHitObject> AMBHitObjectEvent::getHits() const
    { return hits; }

    void AMBHitObjectEvent::setNHit(unsigned ui)
    { nHit = ui; }

    void AMBHitObjectEvent::setL1ID(unsigned ui)
    { l1ID = ui; }

    void AMBHitObjectEvent::setHits(std::vector<AMBHitObject> v)
    { hits = v; }

    void AMBHitObjectEvent::pushHit(AMBHitObject aro)
    { hits.push_back(aro); }

  }
}

