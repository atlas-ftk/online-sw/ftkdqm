#include "ftkdqm/gnamFTKTowerHisto.h"
#include "ftkdqm/gnamFTKTrack.h"
#include <boost/algorithm/string.hpp>
#include <gnam/gnamutils/GnamUtils.h> // global include file for libraries


using namespace daq::ftk;
//=======================================
//
//=======================================
void gnamFTKTowerHisto::Initialize(std::vector< uint16_t > tower_id)
{
 m_n_tower      = tower_id.size(); //64;
//ERS_LOG("tower size "<<m_n_tower);
m_slices_tower = tower_id;
 m_histos.resize(m_n_tower);
}
//=======================================
//
//=======================================
gnamFTKTowerHisto::gnamFTKTowerHisto(const std::string name, const std::string title,
    Int_t nbinsx, Double_t xlow, Double_t xup,
    std::vector< uint16_t > tower_id)
{
  Initialize(tower_id);
//ERS_LOG("towerid-" << tower_id);
  std::vector<std::string> title_parts; // split title of form title;xlabel;ylabel
  boost::split(title_parts, title, boost::is_any_of(";"));
  title_parts.resize(3, "");            // make sure title_parts has correct size if too few/many ";" are found
  int i_histo(0);

  for(int i_tower=0; i_tower<m_n_tower; i_tower++){
       
    i_histo = i_tower ;
  m_histos[i_histo] = new GnamHisto(Form("%s_Tid_%02i", name.c_str(), i_tower), 
          Form(Form("%s: towerid- %i;%s;%s", title_parts[0].c_str(),i_tower, title_parts[1].c_str(), title_parts[2].c_str())),
          nbinsx, xlow, xup,
     GnamHisto::INTBIN); 

}
}
 
 
//=======================================
//
//=======================================
gnamFTKTowerHisto::gnamFTKTowerHisto(const std::string name, const std::string title,
    Int_t nbinsx, Double_t xlow, Double_t xup,
    Int_t nbinsy, Double_t ylow, Double_t yup,
    std::vector< uint16_t > tower_id)
{
  Initialize(tower_id);
//ERS_LOG("towerid-" << tower_id);
  std::vector<std::string> title_parts; // split title of form title;xlabel;ylabel
  boost::split(title_parts, title, boost::is_any_of(";"));
  title_parts.resize(3, "");            // make sure title_parts has correct size if too few/many ";" are found
  int i_histo(0);

  for(int i_tower=0; i_tower<m_n_tower; i_tower++){
       
    i_histo = i_tower ;
  m_histos[i_histo] = new GnamHisto(Form("%s_Tid_%02i", name.c_str(), i_tower), 
          Form(Form("%s: towerid- %i;%s;%s", title_parts[0].c_str(),i_tower, title_parts[1].c_str(), title_parts[2].c_str())),
          nbinsx, xlow, xup, nbinsy, ylow, yup,
     GnamHisto::INTBIN); 

}
}

//========================================
//
//==========================================
gnamFTKTowerHisto::~gnamFTKTowerHisto()
{
  int n_histos = m_histos.size();
//ERS_LOG("histo size-" << n_histos);
  for(int i_histo=0; i_histo<n_histos; i_histo++){
    delete m_histos[i_histo]; m_histos[i_histo] = NULL;
  }}
 
//=======================================
//
//=======================================
Int_t gnamFTKTowerHisto::Fill(Double_t x, uint16_t towerid, Double_t w)
{

  GnamHisto* histo = GetHisto(towerid);
 if(histo){
      return histo->Fill(x, w);
 }
 else{
	return -1;
 }
}

Int_t gnamFTKTowerHisto::Fill(Double_t x, Double_t y, uint16_t towerid, Double_t w)
{

  GnamHisto* histo = GetHisto(towerid);
 if(histo){
      return histo->Fill(x, y, w);
 }
 else{
	return -1;
 }
}

//=======================================
//
//=======================================
GnamHisto* gnamFTKTowerHisto::GetHisto(uint16_t towerid)
{
int i_tower (-1);
  for(int i=0; i<m_n_tower; i++){
	if( m_slices_tower[i] == towerid){
       //ERS_LOG("towerid="<<m_slices_tower[i]);
	 i_tower = m_slices_tower[i];
	break; }}
int i_histo = i_tower;
  if(towerid >= 0){
	  return m_histos[i_histo];}
 
  else
      return 0;
 
}
//=======================================
//
//==========================================
void gnamFTKTowerHisto::Register(std::vector<GnamHisto*> *gnam_histolist)
{
  for(GnamHisto* hist : m_histos)
    gnam_histolist->push_back(hist);}

//=======================================
//
//=======================================
void gnamFTKTowerHisto::PrepareForPublish(void)
{ //ERS_LOG("preparing for publish");
}


