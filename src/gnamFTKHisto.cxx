// GNAM: 
#include <gnam/gnamutils/GnamUtils.h> // global include file for libraries
#include <gnam/gnamutils/GnamHisto.h>
// TDAQ: 
#include <config/Configuration.h> // OKS DB entry point
#include <dal/util.h>             // for DB object casting

// my includes
#include "ftkdqm/gnamFTKEventFragment.h"			//  definition of the class correspoding to my event
#include "ftkdqm/gnamFTKTrack.h"							//  FTK track class
#include "ftkdqm/gnamFTKExceptions.h"					//  gnam FTK exception 
#include "ftkdqm/gnamFTKTowerHisto.h"         //  eta phi tower histogram class
#include "gnamFTKLibdal/gnamFTKHistoLib.h"  //  definition of the DB object correspoding to my library

// std
#include <stdlib.h>
#include <errno.h>
#include <string>
#include <vector>
#include <sstream> 

// root
#include <TH1.h>
#include <TH2.h>
#include <TProfile.h>
#include <TString.h> // Form
#include <TFile.h> // Form

/* 
 * GNAM
 * Use *one* instance for each one of your data histograms; their addresses
 * will be added to a list after calling "bookHisto" and the list cannot
 * change until next run.
 */
static GnamHisto *h1_d0,
                 *h1_z0,
                 *h1_phi0,
                 *h1_eta,
                 *h1_pt,
                 *h1_chiSq,
                 *h1_nPix,
                 *h1_nSct,
                 *h1_nTrks,
                 *h1_layermap,
		 *h1_tower;
	
static GnamHisto *h2_phi_d0,
                 *h2_phi_pt,
                 *h2_phi_eta,
                 *h2_eta_z0,
                 *h2_lvl1id_nTrks,
		 *h2_lb_nTrks;
//		 
// for distributions in eta phi towers,
// eta phi bins are specified via oks database
static daq::ftk::gnamFTKTowerHisto *h1_tower_d0,
  *h1_tower_z0,
  *h1_tower_chiSq,
  *h1_tower_pt,
  *h1_tower_layermap;

static daq::ftk::gnamFTKTowerHisto *h2_tower_PhiEta,
		*h2_tower_lb_nTrcks;

static GnamHisto *p1_lvl1id_nTrks,
		 *p1_lb_nTrks,
                 *p1_phi_d0,
                 *p1_eta_z0;
/*
 * GNAM
 * configuration parameters
 */
//int tower;
static int nBins1D,
           nBins2DX, 
           nBins2DY,
           nTracksMax;
static std::vector<uint16_t> towerid;

static double ptMax,
           etaMax,
           phi0Max,
           z0Max,
           d0Max,
           chi2Max;

static std::vector< double > eta_slices, phi_slices; // eta and phi bins for gnamFTKTowerHistos

// flag for histograms with axis limits to be determined during runtime
static bool firstCall = true; 

/*
 * GNAM
 * Use this syntax for the histogram list. This will assure 
 * a clean library unloading. Look at the sintax also in
 * the "internal_init" and "end" functions  
 */
static std::vector<GnamHisto*> *histolist = NULL;
/*
 * GNAM
 * "MyObject" will point to the contents of the branch "MyClassBranch",
 * which was filled by the decoding routine.
 * For efficiency reasons, it is recommended to declare it static.
 * Declare here, so it can be set to NULL in Cleanup.
 *
 */
static const daq::ftk::gnamFTKEventFragment *myFragment = NULL;



/*
 * Cleanup
 * Delete all pointers. Don't just loop over histolist and delete as it will cause
 * problems with TowerHistos destructor"
 */

void Cleanup(void)
{
  ERS_LOG("gnamFTKHisto::Cleanup()");
  if (histolist != NULL) { 
   // histolist->clear();
    delete histolist; histolist = NULL;

    delete h1_d0; h1_d0 = NULL;
    delete h1_z0; h1_z0 = NULL;
    delete h1_phi0; h1_phi0 = NULL;
    delete h1_eta; h1_eta = NULL;
    delete h1_pt; h1_pt = NULL;
    delete h1_chiSq; h1_chiSq = NULL;
    delete h1_nPix; h1_nPix = NULL;
    delete h1_nSct; h1_nSct = NULL;
    delete h1_nTrks; h1_nTrks = NULL;
    delete h1_layermap; h1_layermap = NULL;
    delete h1_tower; h1_tower= NULL;

    delete h2_phi_d0; h2_phi_d0 = NULL;
    delete h2_phi_pt; h2_phi_pt = NULL;
    delete h2_phi_eta; h2_phi_eta = NULL;
    delete h2_eta_z0; h2_eta_z0 = NULL;
    delete h2_lvl1id_nTrks; h2_lvl1id_nTrks = NULL;
    delete h2_lb_nTrks; h2_lb_nTrks = NULL;

    delete h1_tower_d0; h1_tower_d0 = NULL;
    delete h1_tower_z0; h1_tower_z0 = NULL;
    delete h1_tower_chiSq; h1_tower_chiSq = NULL;
    delete h1_tower_pt; h1_tower_pt = NULL;
    delete h1_tower_layermap; h1_tower_layermap = NULL;
    delete h2_tower_PhiEta; h2_tower_PhiEta = NULL;
    delete h2_tower_lb_nTrcks; h2_tower_lb_nTrcks = NULL;

    delete p1_lvl1id_nTrks; p1_lvl1id_nTrks = NULL;
    delete p1_lb_nTrks; p1_lb_nTrks = NULL;
    delete p1_phi_d0; p1_phi_d0 = NULL;
    delete p1_eta_z0; p1_eta_z0 = NULL;
  }
  // set NULL or GNAM_BRANCH_FILL won't set to new EventFragment instance
  // after doing Unconfig -> Config -> Start in RC
  myFragment = NULL;
 }

/* 
 * GNAM
 * "initDB" function: called once just after the library has been loaded
 * in case of configuration from OKS DB.
 * You should collect your configuration parameters from the DB.
 */
  extern "C" void
initDB (Configuration * confDB, const gnamdal::GnamLibrary * library)
{
  ERS_LOG("gnamFTKHisto::initDB()");
  /* 
   * In order to read your configuration parameters, you 
   * need to downcast the abstract opject "library" to 
   * the type of your library 
   */
  const gnamFTKLibdal::gnamFTKHistoLib * histoLib =
    confDB->cast<gnamFTKLibdal::gnamFTKHistoLib,
    gnamdal::GnamLibrary>(library);

  nBins1D         = histoLib->get_nBins1D();
  nBins2DX        = histoLib->get_nBins2DX();
  nBins2DY        = histoLib->get_nBins2DY();

  ptMax           = histoLib->get_ptMax();
  etaMax          = histoLib->get_etaMax();
  phi0Max         = histoLib->get_phi0Max();
  z0Max           = histoLib->get_z0Max();
  d0Max           = histoLib->get_d0Max();
  chi2Max         = histoLib->get_chi2Max();
  nTracksMax      = histoLib->get_nTracksMax();
  
  towerid         = histoLib->get_towerid();
  eta_slices      = histoLib->get_etaSlices();
  phi_slices      = histoLib->get_phiSlices();

    
/* std::stringstream out_ss;
  out_ss << "Histogram binning parameters:"
    << "\nnBins1D = " << nBins1D
    << "\nnBins2DX = " << nBins2DX
    << "\nnBins2DY = " << nBins2DY
    << "\nnTracksMax = " << nTracksMax
    << "\nptMax = " << ptMax
    << "\netaMax = " << etaMax
    << "\nphi0Max = " << phi0Max
    << "\nz0Max = " << z0Max
    << "\nd0Max = " << d0Max
    << "\nchi2Max = " << chi2Max
    << "\neta tower bins: ";
   for(double eta: eta_slices)
    out_ss << eta << ", ";
  out_ss << "\nphi tower bins: ";
  for(double phi: phi_slices)
    out_ss << phi << ", ";
out_ss << "\ntower: ";
  for (uint16_t tower: towerid)
     out_ss << tower << ", ";*/

//  ERS_LOG(out_ss.str());
}

/* 
 * GNAM
 * "end" function: called once just before the library will be unloaded.
 * You should put here your final clean-up tasks.
 */
  extern "C" void
end (void)
{
  ERS_LOG("gnamFTKHisto::end()");

  Cleanup();
}

/* 
 * GNAM
 * "fillHisto" function: look for your branch (or branches)
 * in the data storage and fill your histograms.
 */
  extern "C" void
fillHisto (void)
{
  if(firstCall)
    ERS_LOG("gnamFTKHisto::fillHisto() first call");

   try {
    GNAM_BRANCH_FILL ("gnamFTKEventFragmentBranch", daq::ftk::gnamFTKEventFragment, myFragment);
  }
  catch (daq::gnamlib::NotExistingBranch &exc) {
    throw daq::gnamlib::CannotFindBranch (ERS_HERE, "gnamFTKEventFragmentBranch");
  }
  
  for( const auto& track : *myFragment->getTracks() ){
    h1_d0->     Fill( track.GetD0()     );
    h1_z0->     Fill( track.GetZ0()     );
    h1_phi0->   Fill( track.GetPhi0()   );
    h1_chiSq->  Fill( track.GetChiSq()  );
    h1_pt->     Fill( track.GetPt()     );
    h1_eta->    Fill( track.GetEta()    );
    h1_nPix->   Fill( track.GetNPixHits()    );
    h1_nSct->   Fill( track.GetNSctHits()    );
    h1_tower->Fill(track.GetTower());

    h2_phi_d0->     Fill( track.GetPhi0(), track.GetD0()    );
    h2_phi_pt->     Fill( track.GetPhi0(), track.GetPt() );
    h2_phi_eta->    Fill( track.GetPhi0(), track.GetEta()   );
    h2_eta_z0->     Fill( track.GetEta(),  track.GetZ0()    );
   
    p1_phi_d0->     Fill( track.GetPhi0(), track.GetD0()    );
    p1_eta_z0->     Fill( track.GetEta(),  track.GetZ0()    );

    h2_tower_PhiEta-> Fill(track.GetPhi0(), track.GetEta(), track.GetTower());
    h1_tower_d0->     Fill( track.GetD0(),  track.GetTower() );
    h1_tower_z0->     Fill( track.GetZ0(),  track.GetTower() );
    h1_tower_chiSq->  Fill( track.GetChiSq(), track.GetTower()  );
    h1_tower_pt->     Fill( track.GetPt(),  track.GetTower() );
  
  h2_tower_lb_nTrcks-> Fill(myFragment->getLumiBlock(), myFragment->getNumTracks(), track.GetTower());
 //ERS_LOG("NpixHits"<< track.GetNPixHits());
    for(uint8_t layer=0; layer<12; layer++){
      h1_layermap->Fill(layer, track.GetHitInLayer(layer));
      h1_tower_layermap->Fill(layer, track.GetTower(),   track.GetHitInLayer(layer));
    } 
 
}
 h1_nTrks->Fill( myFragment->getNumTracks() );
 
 uint32_t lvl1_id(myFragment->getL1ID());
  if(firstCall){ // adapt axis to lvl1id range
   // ERS_LOG("Changing h2_lvl1id_nTrks Xaxis range to [" << double(lvl1_id)-.5 << ", " << double(lvl1_id)+.5 << "]");
    firstCall = false;
    TAxis* axis = h2_lvl1id_nTrks->gTH2()->GetXaxis();
    axis->Set( 100, double(lvl1_id)-1.5 , double(lvl1_id)+1.5 );
    axis = p1_lvl1id_nTrks->gTH1()->GetXaxis();
    axis->Set( 100, double(lvl1_id)-1.5 , double(lvl1_id)+1.5 );
  }
  h2_lvl1id_nTrks->Fill( lvl1_id, myFragment->getNumTracks()); 
  p1_lvl1id_nTrks->Fill( lvl1_id, myFragment->getNumTracks());
  
  uint32_t lumi_block(myFragment->getLumiBlock());
  h2_lb_nTrks->Fill( lumi_block, myFragment->getNumTracks()); 
  p1_lb_nTrks->Fill( lumi_block, myFragment->getNumTracks());
 

 
}

/* 
 * GNAM
 * "startOfRun" function: called once every start of run;
 * here you can prepare your library for the histogram filling.
 * "run" is the run number. For examples see gnamFTK and gnam::GnamHisto
 */
  extern "C" const std::vector<GnamHisto*> *
startOfRun (int /*run*/,std::string /*type*/)
{
  ERS_LOG("gnamFTKHisto::startOfRun()");
  ERS_ASSERT (histolist == NULL);
  // Use this syntax
  histolist = new std::vector<GnamHisto *>;
  firstCall = true;

//int tower = static_cast<int>(towerid) ;
 //needs to have this path structure /SHIFT/, /EXPERT/, /RUNSTAT/, or /DEBUG/ + SUBPATH/
  const std::string expertPath = "/EXPERT/FTK/"; 

  h1_d0 = new GnamHisto(Form("%sh1_inclusive_d0", expertPath.c_str()),
      Form(";d0 [mm];#events"),
      100, -d0Max, d0Max, 
      GnamHisto::INTBIN);
  histolist->push_back(h1_d0);

  h1_z0 = new GnamHisto(Form("%sh1_inclusive_z0", expertPath.c_str()),
      Form(";z0 [mm];#events"),
      250, -z0Max, z0Max, 
      GnamHisto::INTBIN);
  histolist->push_back(h1_z0);

  h1_phi0 = new GnamHisto(Form("%sh1_inclusive_phi0", expertPath.c_str()),
      Form(";#phi_{0} [rad];#events"),
      100, -phi0Max, phi0Max, 
      GnamHisto::INTBIN);
  histolist->push_back(h1_phi0);
  
 h1_chiSq = new GnamHisto(Form("%sh1_inclusive_chiSq", expertPath.c_str()),
      Form(";#chi^{2};#events"),
      chi2Max, 0, chi2Max, 
      GnamHisto::INTBIN);
  histolist->push_back(h1_chiSq);
  
  h1_pt = new GnamHisto(Form("%sh1_inclusive_pt", expertPath.c_str()),
      Form(";p_{t} [MeV];#events"),
      100, 0, ptMax, 
      GnamHisto::INTBIN);
  histolist->push_back(h1_pt);
  
  h1_eta = new GnamHisto(Form("%sh1_inclusive_eta", expertPath.c_str()),
      Form(";#eta;#events"),
      100, -3, 3, 
      GnamHisto::INTBIN);
  histolist->push_back(h1_eta);
 
  
 h1_nPix = new GnamHisto(Form("%sh1_inclusive_nPix", expertPath.c_str()),
      Form(";# Pixel hits;#events"),
      100, -.5, 4.5, 
      GnamHisto::INTBIN);
  histolist->push_back(h1_nPix);

  h1_nSct = new GnamHisto(Form("%sh1_inclusive_nSct", expertPath.c_str()),
      Form(";# SCT hits;#events"),
      100, -.5, 8.5, 
      GnamHisto::INTBIN);
  histolist->push_back(h1_nSct);

  h1_nTrks = new GnamHisto(Form("%sh1_inclusive_nTrks", expertPath.c_str()),
      Form(";#tracks/event;#events"),
      nTracksMax, -.5, nTracksMax-0.5,
      GnamHisto::INTBIN);
  histolist->push_back(h1_nTrks);
 
  h1_layermap = new GnamHisto(Form("%sh1_inclusive_layermap", expertPath.c_str()),
      Form(";layer;#hits"),
      12, -.5, 11.5,
      GnamHisto::INTBIN);
  histolist->push_back(h1_layermap);

  h2_phi_d0 = new GnamHisto(Form("%sh2_inclusive_phi_d0",expertPath.c_str()), Form(";#phi_{0} [rad];d_{0} [mm]"),
      100, -phi0Max, phi0Max,
      100 , -d0Max, d0Max, 
      GnamHisto::FLOATBIN);
  histolist->push_back(h2_phi_d0);
  p1_phi_d0 = new GnamHisto(Form("%sp1_phi_d0",expertPath.c_str()), Form(";#phi_{0} [rad];d_{0} [mm]"),
      100, -phi0Max, phi0Max, 
      GnamHisto::FLOATBIN);
  histolist->push_back(p1_phi_d0);

  h2_phi_pt = new GnamHisto(Form("%sh2_inclusive_phi_pt",expertPath.c_str()), 
      Form(";#phi_{0} [rad];p_{t} [MeV]"),
      100, -phi0Max, phi0Max,
      100, 0,   ptMax, 
      GnamHisto::INTBIN);
  histolist->push_back(h2_phi_pt);

  h2_phi_eta = new GnamHisto(Form("%sh2_inclusive_phi_eta",expertPath.c_str()), 
      Form(";#phi_{0} [rad];#eta"),
      100, -phi0Max, phi0Max,
      100, -3, 3, 
      GnamHisto::INTBIN);
  histolist->push_back(h2_phi_eta);

  h2_eta_z0 = new GnamHisto(Form("%sh2_inclusive_eta_z0",expertPath.c_str()), 
      Form(";#eta;z_{0} [mm]"),
      100, -3, 3,
      400, -z0Max, z0Max, 
      GnamHisto::FLOATBIN);
  histolist->push_back(h2_eta_z0);
  p1_eta_z0 = new GnamHisto(Form("%sp1_eta_z0",expertPath.c_str()), 
      Form(";#eta;z_{0} [mm]"),
      100, -3, 3, 
      GnamHisto::FLOATBIN);
  histolist->push_back(p1_eta_z0);


  h2_lvl1id_nTrks = new GnamHisto(Form("%sh2_inclusive_lvl1id_nTrks", expertPath.c_str()), 
      ";lvl1 id;number of FTK tracks",
      100,0,1, nTracksMax, -0.5, nTracksMax-0.5, 
      GnamHisto::FLOATBIN);
  // abuse GnamHisto as TProfile, see prePublish() for processing
  p1_lvl1id_nTrks = new GnamHisto(Form("%sp1_lvl1id_nTrks", expertPath.c_str()), 
      ";lvl1 id;average number of FTK tracks",
      100,0,1, 
      GnamHisto::FLOATBIN);
 h2_lb_nTrks = new GnamHisto(Form("%sh2_inclusive_lb_nTrks", expertPath.c_str()), 
      ";lb;number of FTK tracks",
      10000,0,10000, 1000, -0.5, nTracksMax-0.5, 
      GnamHisto::FLOATBIN);
 h1_tower = new GnamHisto(Form("%sh1_inclusive_tower", expertPath.c_str()), 
      ";towerID;#events",
      64,-0.5,63.5, 
      GnamHisto::INTBIN);
 histolist->push_back(h1_tower);

 
 // abuse GnamHisto as TProfile, see prePublish() for processing
  p1_lb_nTrks = new GnamHisto(Form("%sp1_lb_nTrks", expertPath.c_str()), 
      ";lb;average number of FTK tracks",
      10000,0,10000, 
      GnamHisto::FLOATBIN);


  h2_lvl1id_nTrks->gTH2()->SetCanExtend(TH1::kXaxis); // new way used in root6
  p1_lvl1id_nTrks->gTH1()->SetCanExtend(TH1::kXaxis); 

  histolist->push_back(h2_lvl1id_nTrks);
  histolist->push_back(p1_lvl1id_nTrks);
  histolist->push_back(h2_lb_nTrks);
  histolist->push_back(p1_lb_nTrks);
 
 ERS_LOG(std::to_string(histolist->size()) + " regular histos booked");

 //ERS_LOG("tower in hist=" << towerid);
 
  // variables in eta phi bins, don't forget to Register and PrepareForPublish in prePublish
h1_tower_d0 = new daq::ftk::gnamFTKTowerHisto(
      Form("%sh1_Tower_d0", expertPath.c_str()), "d0;d0 [mm];#events", 
      100, -d0Max, d0Max, towerid);
  h1_tower_d0->Register(histolist);
  h1_tower_z0 = new daq::ftk::gnamFTKTowerHisto(
      Form("%sh1_Tower_z0", expertPath.c_str()), "z0;z0 [mm];#events", 
      250, -z0Max, z0Max, towerid);
  h1_tower_z0->Register(histolist);
  h1_tower_chiSq = new daq::ftk::gnamFTKTowerHisto(
      Form("%sh1_Tower_chiSq", expertPath.c_str()), "#chi^{2};#chi^{2};#events", 
      chi2Max, 0, chi2Max, towerid);
  h1_tower_chiSq->Register(histolist);
  h1_tower_pt = new daq::ftk::gnamFTKTowerHisto(
      Form("%sh1_Tower_pt", expertPath.c_str()), "p_{t};p_{t} [MeV];#events", 
      100, 0, ptMax, towerid);
  h1_tower_pt->Register(histolist);

  h1_tower_layermap = new daq::ftk::gnamFTKTowerHisto(
      Form("%sh1_Tower_layermap", expertPath.c_str()), "layermap;layer;#hits", 
      12, -0.5, 11.5, towerid);
  h1_tower_layermap->Register(histolist);

 h2_tower_PhiEta = new daq::ftk::gnamFTKTowerHisto(
      Form("%sh2_Tower_phi_eta", expertPath.c_str()), 
      ";#phi;#eta",
      16, -3.15, 3.15, 4,-3,3, towerid);
 h2_tower_PhiEta->Register(histolist);

 h2_tower_lb_nTrcks = new daq::ftk::gnamFTKTowerHisto(
      Form("%sh2_Tower_lb_nTrcks", expertPath.c_str()), 
      ";lb;nTracks",
      10000,0,10000, 100, -0.5, 100, towerid);
 h2_tower_lb_nTrcks->Register(histolist);

  
  ERS_LOG(std::to_string(histolist->size()) + " histos booked, including TowerHistos");
  return histolist;
}

/* 
 * GNAM
 * "prePublish" function: here you can fill special histograms just
 * before the publication.
 */
  extern "C" void
prePublish (bool endOfRun)
{
  //ERS_LOG("gnamFTKHisto::prePublish()");

  // set GnamHisto p1_lvl1id_nTrks to ProfileX of h2_lvl1id_nTrks
  p1_lvl1id_nTrks->gTH1()->Reset(); 
  TH1D* h_temp = (TH1D*) h2_lvl1id_nTrks->ProfileX();
  p1_lvl1id_nTrks->Add(h_temp);
  h_temp->Delete(); h_temp = NULL;

  p1_lb_nTrks->gTH1()->Reset(); 
  TH1D* h_tem = (TH1D*) h2_lb_nTrks->ProfileX();
  p1_lb_nTrks->Add(h_tem);
  h_tem->Delete(); h_tem = NULL;

  p1_phi_d0->gTH1()->Reset();
  h_temp = (TH1D*) h2_phi_d0->ProfileX();
  p1_phi_d0->Add(h_temp);
  h_temp->Delete(); h_temp = NULL; 
  
  p1_eta_z0->gTH1()->Reset();
  h_temp = (TH1D*) h2_eta_z0->ProfileX();
  p1_eta_z0->Add(h_temp);
  h_temp->Delete(); h_temp = NULL;

//ERS_LOG("preparing tower histos for publish");

  h1_tower_d0->PrepareForPublish();
  h1_tower_z0->PrepareForPublish();
  h1_tower_chiSq->PrepareForPublish();  
  h1_tower_pt->PrepareForPublish();
  h1_tower_layermap->PrepareForPublish();
  h2_tower_PhiEta->PrepareForPublish();
  h2_tower_lb_nTrcks->PrepareForPublish();

   if (endOfRun) {
    ERS_LOG("end of run");
  }
}

/*
 * GNAM
 * "endOfRun" function: called once every end of run,
 * here you make some clean-up
 */
  extern "C" void
endOfRun (void)
{
  ERS_LOG("gnamFTKHisto::endOfRun()");

  Cleanup();
}

/* 
 * GNAM
 * "DFStopped" function: this function is called after endOfRun, when 
 * the DataFlow is completely stopped. You shoudl not update your
 * histograms here since they aer not going to be published by Gnam. 
 * The function is here mainly for MonaIsa support (IS gathering after the
 * end of run).
 */
  extern "C" void
DFStopped (void)
{
  ERS_LOG("gnamFTKHisto::DFStopped()");
}


/*
 * GNAM
 * "customCommand" function: here you will receive custom command from OH.
 * You will have also the parameters of your command 
 */
  extern "C" void
customCommand (const GnamHisto*  /*histo*/ , int /*argc*/ , const char * const* /*argv*/)
{
  ERS_LOG("gnamFTKHisto::customCommand()");

}
