#include "ftkdqm/AMBRoadObject.h"

#include <iostream>
#include <iomanip>

namespace daq {
  namespace ftk {
    
    AMBRoadObject::AMBRoadObject()
    { }
    
    AMBRoadObject::AMBRoadObject(unsigned data,
		    unsigned bitMap,
		    unsigned numberOfHits,
		    std::vector<unsigned> missingLayers,
		    unsigned chip,
		    unsigned link,
		    unsigned lamb,
		    unsigned patternAddress){ 
      this->data = data;
      this->bitMap = bitMap;
      this->numberOfHits = numberOfHits;
      this->missingLayers = missingLayers;
      this->chip = chip;
      this->link = link;
      this->lamb = lamb;
      this->patternAddress = patternAddress;
      this->geoAddress = (lamb<<4)+(link<<2)+chip;
    }

    AMBRoadObject::AMBRoadObject(const AMBRoadObject &obj){ 
      this->data = obj.data;
      this->bitMap = obj.bitMap;
      this->numberOfHits = obj.numberOfHits;
      this->missingLayers = obj.missingLayers;
      this->chip = obj.chip;
      this->link = obj.link;
      this->lamb = obj.lamb;
      this->patternAddress = obj.patternAddress;
      this->geoAddress = (obj.lamb<<4)+(obj.link<<2)+obj.chip;
    }

    AMBRoadObject::~AMBRoadObject()
    { }

    unsigned AMBRoadObject::getData() const
    { return data; }
    
    unsigned AMBRoadObject::getBitMap() const
    { return bitMap; }
    
    unsigned AMBRoadObject::getNumberOfHits() const
    { return numberOfHits; }
    
    std::vector<unsigned> AMBRoadObject::getMissingLayers() const
    { return missingLayers; }
    
    unsigned AMBRoadObject::getChip() const
    { return chip; }
    
    unsigned AMBRoadObject::getLink() const
    { return link; }
    
    unsigned AMBRoadObject::getLamb() const
    { return lamb; }
    
    unsigned AMBRoadObject::getPatternAddress() const
    { return patternAddress; }

    unsigned AMBRoadObject::getGeoAddress() const
    { return geoAddress; }

    void AMBRoadObject::print()
    { 
      std::cout << std::setw(8) 
		<< std::setfill('0') 
		<< std::right 
		<< std::hex 
		<< data
		<<" (LAMB:"<<lamb
		<<", link:"<<link
		<<", chip:"<<chip
		<<", pattern address:"<<patternAddress
		<<", bitMap:"<<bitMap
		<<", number of hits:"<<numberOfHits;
      if(missingLayers.size()!=0){
	std::cout<<"(missing:";
	for(int iml=0; iml<missingLayers.size(); iml++) std::cout<<" "<<missingLayers.at(iml) ;
	std::cout<<")";
      }
      std::cout<<")"<<std::endl;;
    }
  }

}

